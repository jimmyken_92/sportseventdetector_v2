# Sports Event Detector


### How do I get set up?

* Get football matches videos and event data (.json files)
* Requirements for executing the code successfully, are defined in the requirements.txt file
* Therefore one should install all the libraries placed within the .txt file before moving to the next step


### Initial Step - Create Dataset

* Open params.ini file 
* Modify parameters inside of section [Dataset Creator]
* data_raw is the folder from where we pick the mp4 and json files
* event_ids include the ids of the selected events. Go to [wyscout](https://apidocs.wyscout.com/matches-wyid-events#6-details-of-formations-object)
* Set Consolidate True or False
* dataset is the folder in which extracted images will be stored
* sequence_length defines the number of seconds. Have in mind that we want 2/3 of this amount of time before the event and 1/3 after
* consolidate_period is the amount of time in which two events can happen in sequence

**run python main.py -c**


### Training and Testing afterwards ###

* Open params.ini file
* Modify parameters inside of section [general]
* model_name: pick a model among the available ones
* target_names: labels of the corresponding event_ids from the [Dataset Creator] Section
* train_params: parameters that have to do with the model training such as no of epochs, data shuffling etc
* compile_params: parameters that have to do with the model compile procedure such as loss, optimizer etc
* val_pc: set to 0.2 so as to have 60% of data for training, 20% for validation and 20% for testing
* test_matches: Define match names that will be used for testing
* dataset: the folder in which extracted images are already stored
* batch_size: size of each batch. Values like [32,64,128,256,512,1024]. Have in mind that increasing this value demands higher computational standards
* norm_factor: When using resnet50/inceptionv3 set to 1. Otherwise set to 255
* past_frames: how many past frames to go back from the desired event
* skip_frames: time interval
* full_event_skip: Set to nonzero value so as not to have the full_event. Set to 0 and you will have the full_event returned
* epochs_after_unfreeze: Set this value to desired number of epochs for a model such resnet50/inceptionv3 to train all their layers. Otherwise set to 0
* use_preprocessor: Set to True or False

**run python main.py**

## Inference
Infer events: `infer.py`  
Infer the camera view: `infer_cam_view.py`  
Infer whether the scoreboard is present or not: `infer_sb.py`   


### Only Testing
**run python main.py -t FOLDER_NAME(in which trained model is stored)**


**Working on...**



