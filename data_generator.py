import tensorflow as tf
import numpy as np
import os
import cv2
from typing import List
import random
from os import listdir
from os.path import isfile, join
import ast
from collections import deque

if tf.__version__[0] == '2':
	from tensorflow.keras.utils import Sequence, to_categorical
	from tensorflow.keras.applications.resnet50 import preprocess_input
else:
	from keras.utils import Sequence
	from keras.utils import to_categorical
	from keras.applications.imagenet_utils import preprocess_input


def split_train_val_test(evs, test_match, v_p): # v_p=0.2
	test_evs = []
	# Create test set
	evs2 = evs.copy()
	for ev in evs2:
		if any(match in ev for match in test_match):
			test_evs.append(ev)
			evs.remove(ev)
	# Split into classes
	classes = {}
	for ev in evs2:
		cl = ast.literal_eval(ev.split('_')[0])
		if type(cl) is list:
			cl = cl[0]
		if cl not in classes.keys():
			classes[cl] = [ev]
		else:
			classes[cl].append(ev)
	# Find biggest class
	train_evs = []
	val_evs = []
	max_cl = 0
	for class_events in classes.values():
		split = int(len(class_events) * (1 - v_p))
		if split > max_cl:
			max_cl = split
	# Split into val and train and keep balance
	for class_events in classes.values():
		random.shuffle(class_events)
		split = int(np.ceil(len(class_events) * (1 - v_p)))
		times = int(np.round(max_cl / split))
		train_evs += times * class_events[:split]
		# val_evs += class_events[split:]
		val_evs += class_events[:split]
		print(f'train_evs: {times * class_events[:split]}')

	return train_evs, val_evs, test_evs


def get_predict_event_chunk(frames_queue: deque, skip_frames: int = 5,
							past_frames: int = 6, dim=(224, 224), normalizer=255.0, lstm=False):
	"""Create event chunk for prediction."""
	event = []
	for i in range(past_frames):
		q_idx = -1 - i * skip_frames
		im = frames_queue[q_idx]
		im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
		im = cv2.resize(im, dim, None)

		event.append(im)
	event = np.array(event)
	event = np.flip(event, 0)
	if lstm:
		event = np.expand_dims(event, axis=4)
	else:
		event = np.einsum('jkl->klj', event)

	event = preprocess_input(event, mode='tf')

	return np.array(event)


class DataGenerator(Sequence):
	def __init__(self, list_paths: List, dataset: str, batch_size: int = 32, dim: tuple = (32, 32),
				 shuffle: bool = True, skip: int = 5, past_frames: int = 6, use_color: bool = False,
				 sequential_images: bool = False, normalizer: float = 255.0, full_event_skip: int = 5,
				 use_preprocessor=False):
		self.dim = dim
		self.batch_size = batch_size
		self.list_paths = list_paths
		self.shuffle = shuffle
		self.skip = skip  # Sample every 'skip' frames in sequence
		self.past_frames = past_frames  # How many past frames to use
		self.labels = []
		self.use_color = use_color
		self.dataset = dataset
		self.sequential_images = sequential_images
		self.normalizer = normalizer
		self.full_event_skip = full_event_skip
		self.use_preprocessor = use_preprocessor
		self.on_epoch_end()

	def get_full_event(self, event_id):
		full_event = []
		event_name = self.list_paths[event_id]
		images = [f for f in listdir(self.dataset + event_name) if isfile(join(self.dataset + event_name, f))]
		for start_frame in range(0, images.__len__() - self.past_frames * self.skip - 1, self.full_event_skip):
			event = self.get_event_chunk(event_name, start_frame)
			event = np.stack(event, axis=0)
			full_event.append(event)
		full_event = np.stack(full_event, axis=0)
		if self.use_preprocessor:
			full_event = preprocess_input(full_event, mode='tf')
		if not self.use_color and not self.sequential_images:
			full_event = np.expand_dims(full_event, axis=4)
		return full_event

	def get_event_chunk(self, path, start_frame):
		event = []
		for i in range(start_frame, start_frame + self.skip * self.past_frames, self.skip):
			if not self.use_color:
				im = cv2.imread(self.dataset + path + f'/{i}.jpg', 0)
			else:
				im = cv2.imread(self.dataset + path + f'/{i}.jpg')
			im = cv2.resize(im, self.dim, None) / self.normalizer
			event.append(im)
		if self.sequential_images:
			event = np.einsum('jkl->klj', event)
		return np.array(event)

	def __len__(self):
		return int(np.floor(len(self.list_paths) / self.batch_size))

	def __getitem__(self, index):
		list_paths = self.list_paths[index * self.batch_size:(index + 1) * self.batch_size]
		y = self.labels[index * self.batch_size:(index + 1) * self.batch_size, :]

		x = []
		for path in list_paths:
			number_of_ims = os.listdir(self.dataset + path).__len__() - 1
			start_frame = np.random.randint(number_of_ims - self.skip * self.past_frames)
			event = self.get_event_chunk(path, start_frame)
			x.append(event)

		x = np.array(x)
		if not self.use_color and not self.sequential_images:
			x = np.expand_dims(x, axis=4)
		if self.use_preprocessor:
			x = preprocess_input(x, mode='tf')
		return x, y

	def on_epoch_end(self):
		random.shuffle(self.list_paths)
		labels = []
		for path in self.list_paths:
			labels.append(ast.literal_eval(path.split('_')[0]))
		if type(labels[0]) is not list:
			self.labels = to_categorical(labels)
		else:
			self.labels = self.to_multicalss_categorical(labels)
		idx = np.argwhere(np.all(self.labels[..., :] == 0, axis=0))
		self.labels = np.delete(self.labels, idx, axis=1)

	def to_multicalss_categorical(self, labels):
		target = np.zeros((labels.__len__(), max(max(labels)) + 1))
		for i in range(labels.__len__()):
			for idx in labels[i]:
				target[i, idx] = 1.0
		return target
