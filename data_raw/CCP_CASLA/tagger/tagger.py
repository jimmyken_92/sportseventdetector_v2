import cv2
import numpy as np
import csv
import yaml
from pathlib import Path
import json


# Config
with open('config.yaml', 'r') as st:
	config = yaml.safe_load(st)

data_dir = Path(config['data_dir'])
pro_name = Path(config['pro_name'])
pro_dir = data_dir/pro_name
video_file = pro_dir/f'{pro_dir.name}.mp4'
json_file = pro_dir/f'event_data.json'

print(f'PROJECT PATH: {pro_dir}')

# Vars
match_events = []
vid_start = 0

# json data
event_data = {}
event_data['events'] = []

cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
cv2.resizeWindow('frame', 600, 600)

cap = cv2.VideoCapture(str(video_file))
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
assert total_frames, 'There are no frames in the video path'
fps = cap.get(cv2.CAP_PROP_FPS)
cap.set(cv2.CAP_PROP_POS_MSEC, vid_start)

speed = 0

# a: Other, Acceleration
# b: Blocked, Low
# c: Pass, Cross
# d: Duel
# g: Goal
# p: Pass, Simple pass
# q: Other, Ball control
# s: Shot
# z: Assist
def draw_event(event, msec):
	event_name = ''
	subevent = ''

	if key == ord('a'):
		event_name = 'Other'
		subevent = 'Acceleration'
		subEventId = 1
	elif key == ord('b'):
		event_name = 'Blocked'
		subevent = 'Low'
		subEventId = 2
	elif key == ord('c'):
		event_name = 'Pass'
		subevent = 'Cross'
		subEventId = 3
	elif key == ord('d'):
		event_name = 'Duel'
		subevent = 'Duel'
		subEventId = 4
	elif key == ord('g'):
		event_name = 'Goal'
		subevent = 'Goal'
		subEventId = 5
	elif key == ord('p'):
		event_name = 'Pass'
		subevent = 'Simple pass'
		subEventId = 6
	elif key == ord('s'):
		event_name = 'Shot'
		subevent = 'Goal shot'
		subEventId = 7
	elif key == ord('q'):
		event_name = 'Other'
		subevent = 'Ball control'
		subEventId = 8
	elif key == ord('z'):
		event_name = 'Assist'
		subevent = 'Assist'
		subEventId = 9
	else:
		return

	if event_name:
		print(f'Event: {event_name}')
		print(f'Time: {msec_pos}')
		event_data['events'].append({
			"id": 1,
			"playerId": 1,
			"teamId": 1,
			"matchId": 50,
			"matchPeriod": "2H",
			"eventSec": msec,
			"eventId": 8,
			"eventName": event_name,
			"subEventId": subEventId,
			"subEventName": subevent,
			"positions": [
				{
					"x": 50,
					"y": 50
				},
				{
					"x": 32,
					"y": 32
				}
			],
			"tags": [
				{
					"id": 1000
				}
			]
		})

while True:
	ret, frame = cap.read()
	help_img = frame.copy()

	if not ret:
		break

	frame_pos = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
	msec_pos = int(cap.get(cv2.CAP_PROP_POS_MSEC))

	cv2.imshow('frame', frame)
	key = cv2.waitKey(speed)

	if key:
		draw_event(event=key, msec=msec_pos)
		match_events.append(event_data)

	if key == ord(','):
		# Back 1 frame
		cap.set(cv2.CAP_PROP_POS_FRAMES, frame_pos - 2)

	if key == ord('\x1b'): break # esc
	# if key == ord('1'): speed = 0
	# if key == ord('2'): speed = 100
	# if key == ord('3'): speed = 25
	# if key == ord('4'): speed = 1

	with open(json_file, 'w') as outfile:
		json.dump(event_data, outfile, indent=2)

cap.release()
cv2.destroyAllWindows()
