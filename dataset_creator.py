import json
import cv2
import os
import multiprocessing
import ast
import shutil
from tqdm import tqdm
import numpy as np
from pathlib import Path
from typing import List, Union
from configparser import ConfigParser


def create_dataset(config_file):
    config = ConfigParser()
    config.read(config_file)

    data_folder = config.get('dataset_creator', 'data_folder')
    consolidate = ast.literal_eval(config.get('dataset_creator', 'consolidate'))
    event_ids = ast.literal_eval(config.get('dataset_creator', 'event_ids'))
    tag_ids = ast.literal_eval(config.get('dataset_creator', 'tag_ids'))
    dataset = config.get('dataset_creator', 'dataset')
    sequence_length = int(config.get('dataset_creator', 'sequence_length'))
    consolidate_period = int(config.get('dataset_creator', 'consolidate_period'))

    match_dirs = get_all_matches(data_folder)
    extractors = [MatchEventExtractor(match, event_ids, tag_ids, dataset, data_folder, consolidate,
                                      sequence_length=sequence_length, consolidate_period=consolidate_period)
                  for match in match_dirs]
    [ex.get_match_events() for ex in extractors]
    processes = [multiprocessing.Process(target=ex.create_image_dataset) for ex in extractors]
    [p.start() for p in processes]
    [p.join() for p in processes]
    delete_folders(config_file)

def delete_folders(config_file):
    # dirName = '/home/jimmy/witcloud/sports/sportseventdetector/'
    config = ConfigParser()
    config.read(config_file)
    dataset = config.get('dataset_creator', 'dataset')
    dir_name = dataset
    dirName = f'{dir_name}'
    listOfEmptyDirs = list()
    for (dirpath, dirnames, filenames) in os.walk(dirName):
        if len(dirnames) <= 89 and len(filenames) <= 89:
            listOfEmptyDirs.append(dirpath)
    # for elem in listOfEmptyDirs:
    #     print(elem)
    listOfEmptyDirs = [dirpath for (dirpath, dirnames, filenames) in os.walk(dirName) if
                       len(dirnames) <= 89 and len(filenames) <= 89]
    delete = []
    for elem in listOfEmptyDirs:
        delete.append(elem)
    delete = np.array(delete)
    # for i in range(0, len(delete)):
    #     print("Deleting:", delete[i])
    #     shutil.rmtree(delete[i], ignore_errors=True)

def delete_dataset(config_file):
    config = ConfigParser()
    config.read(config_file)
    dataset = config.get('dataset_creator', 'dataset')
    shutil.rmtree(dataset)

def filter_events(events, tag, event_id):
    filtered = []
    for event in events['events']:
        if event[tag] == event_id:
            filtered.append(event)
    return filtered

def filter_tags(events, event_id):
    filtered = []
    for event in events['events']:
        tag_list = [t['id'] for t in event['tags']]
        if event_id in tag_list:
            filtered.append(event)
    return filtered

def get_all_matches(data_dir: Union[str, Path] = 'data_raw'):
    _, dirs, _ = os.walk(data_dir).__next__()
    return dirs


class MatchEventExtractor:
    def __init__(self, match_folder: str, ev_ids: List, tag_ids: List, export_folder: str, data_dir: str = 'data_raw',
                 consolidate: bool = True, sequence_length: int = 6, consolidate_period: int = 6):
        self.match_folder = match_folder
        self.ev_ids = ev_ids
        self.tag_ids = tag_ids
        self.export_folder = export_folder
        self.data_dir = data_dir
        self.consolidate = consolidate
        self.sequence_length = sequence_length
        self.consolidate_period = consolidate_period
        self.events = []

        config = ConfigParser()
        config.read(f'{self.data_dir}/{self.match_folder}/params.ini')
        try:
            self.halves_offset = int(config.get('general', 'halves_offset'))
        except:
            self.halves_offset = 0

    def get_match_events(self):
        with open(f'{self.data_dir}/{self.match_folder}/event_data.json', 'r') as f:
            all_events = json.load(f)

        events = []
        for event_id in self.ev_ids:
            events += filter_events(all_events, 'subEventId', event_id)

        print(self.ev_ids)
        tags = []
        for tag_id in self.tag_ids:
            temp = filter_tags(all_events, tag_id)
            #print("temp:",temp)
            for t in temp:
                t['subEventId'] = tag_id
            tags += temp
        #print("tags:",tags)

        events += tags

        h_events = filter_events(all_events, 'matchPeriod', '1H')
        start_1st_half = h_events[0]['eventSec']
        end_1st_half = h_events[-1]['eventSec']
        # start_1st_half = None
        # end_1st_half = None
        h_events = filter_events(all_events, 'matchPeriod', '2H')
        start_2nd_half = h_events[0]['eventSec']

        self._refine_timestamps(events, start_1st_half, end_1st_half, start_2nd_half)
        events = sorted(events, key=lambda k: k['eventSec'])

        no_events = self._find_no_events(events)
        events += no_events

        for event in events:
            event['match'] = self.match_folder
        self.events = events
        if self.consolidate:
            self.events = self._consolidate_events(self.consolidate_period)
        else:
            self.events = [[event] for event in events]
        return self.events

    def create_image_dataset(self):
        data_raw = Path.cwd() / self.data_dir
        dataset = Path.cwd() / self.export_folder

        video_file = data_raw/self.match_folder/f'{self.match_folder}.mp4'
        # if not video_file.exists():
        #     video_file = data_raw/self.match_folder/f'{self.match_folder}.mp4'
        #     if not video_file.exists():
        #         print(f'No video file: {self.match_folder}')
        #         return
        #     else:
        #         print(f'No compressed video file: {self.match_folder}')
        cap = cv2.VideoCapture(str(video_file))
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        frame = cap.read()[1]

        for event in tqdm(self.events):
            # print(event)
            labels = [ev["subEventId"] for ev in event]
            event_folder = dataset/f'{labels.__str__()}_{event[0]["id"]}_{event[0]["match"]}'
            Path(event_folder).mkdir(parents=True, exist_ok=True)  # Make 'events' dir
            timestamp = event[0]['eventSec']
            cap.set(cv2.CAP_PROP_POS_MSEC, (timestamp - int(self.sequence_length * 2 / 3)) * 1000)
            for i in range(self.sequence_length * fps):
                frame = cv2.resize(frame, (256, 256), None)
                cv2.imwrite(str(event_folder / f'{i}.jpg'), frame)

    def _refine_timestamps(self, events, start_1st_half, end_1st_half, start_2nd_half):
        for event in events:
            if event['matchPeriod'] == '2H':
                event['eventSec'] += start_1st_half + end_1st_half + start_2nd_half + self.halves_offset
            if event['matchPeriod'] == '1H':
                event['eventSec'] += start_1st_half
        return events

    # Merges events that happens in a period of 'time_gap'
    def _consolidate_events(self, time_gap):
        no_of_classes = self.ev_ids.__len__()
        sorted_evs = sorted(self.events, key=lambda k: k['eventSec'])
        timestamps = np.array([v['eventSec'] for v in sorted_evs])

        merged_evs = []
        for i in range(timestamps.__len__() - no_of_classes + 1):
            merged_ev = []
            for j in range(no_of_classes):
                if timestamps[i + j] - timestamps[i] < time_gap:
                    merged_ev.append(sorted_evs[i + j])
            merged_evs.append(merged_ev)
        return merged_evs

    def _find_no_events(self, events):
        t = np.array([ev['eventSec'] for ev in events])
        dt = np.diff(t)
        candidates = np.where(dt > 30)
        no_events = []
        for idx in candidates[0]:
            no_event = {'id': events[idx]['id'] * 10,
                        'playerId': 0,
                        'teamId': 0,
                        'matchId': events[idx]['matchId'],
                        'matchPeriod': events[idx]['matchPeriod'],
                        'eventSec': (events[idx]['eventSec'] + events[idx + 1]['eventSec']) / 2,
                        'eventId': 0,
                        'eventName': 'No event',
                        'subEventId': 0,
                        'subEventName': 'No event',
                        'positions': [],
                        'tags': []}
            no_events.append(no_event)
        return no_events
