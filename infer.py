import cv2
import pickle
import threading
import numpy as np
import yaml
import time
from data_generator import get_predict_event_chunk
from collections import deque
from configparser import ConfigParser
import pandas as pd
import traceback
from tools import interface
import sys
from PyQt5 import QtGui, QtWidgets, QtCore, QtTest
from PIL import Image
from fastai.vision import load_learner, pil2tensor
from fastai.vision import Image as fastaiImage
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import os;os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.3
config.gpu_options.visible_device_list = "0"
set_session(tf.Session(config=config))

if tf.__version__[0] == '2':
    from tf.keras.models import load_model
else:
    from keras.models import load_model


class Infer(QtCore.QThread):
    def __init__(self, param_file='params.yaml', show_output=True,
                 show_cv=False, write_csv=False):
        QtCore.QThread.__init__(self)
        self.show_output = show_output
        self.show_cv = show_cv
        self.write_csv = write_csv
        with open(param_file, 'r') as stream:
            config = yaml.safe_load(stream)
        self.model_name = config['inference']['model_name']
        self.l_buff_div = config['inference']['sb']['l_buff_div']
        self.start_time = config['inference']['start_time']
        vid_path = config['inference']['vid_path']
        self.pred_rate = config['inference']['pred_rate']
        a_list = np.array(config['inference']['a'])
        b_list = np.array(config['inference']['b'])

        model_config = ConfigParser()
        model_config.read(f'models/{self.model_name}/params.ini')
        self.past_frames = int(model_config.get('general', 'past_frames'))
        self.skip_frames = int(model_config.get('general', 'skip_frames'))
        self.labels = np.array(config['inference']['labels'])
        self.ui_labels = ['', 'corner', 'throw_in', 'shot', 'goal', 'yellow_card']
        print(self.labels)
        self.threshold_list_event_detection = np.array(config['inference']['thr'])
        self.threshold_list_forecasting = np.array(config['inference']['thr_pred'])
        self.graph = tf.get_default_graph()
        self.a = np.diag(a_list)
        self.b = np.eye(6) - self.a
        self.b2 = np.concatenate((-b_list.reshape(6,1), self.b), axis=1)
        self.frame_num = []
        self.frame_pred = []
        self.graph = tf.get_default_graph()
        self.threshold_list_event_detection = np.array(config['inference']['thr'])
        self.threshold_list_forecasting = np.array(config['inference']['thr_pred'])
        self.threads = []

        print('Loading detection model...')
        self.model = load_model(f'models/{self.model_name}/model.h5')

        print('Loading cam_view_model...')
        self.camview_model = load_learner('resources', file='learn_export_camview.pkl')
        self.camview_model.to_fp32()
        print(self.camview_model.data.classes)

        print('Loading filtering model...')
        self.model_filter = load_model('resources/model3.h5')

        print('Loading scoreboard model...')
        self.sb_model = load_learner('resources', file='learn_export_sb.pkl')
        self.sb_model.to_fp32()
        print(self.sb_model.data.classes)

        print('Loading throwin model...')
        self.ti_model = load_learner('resources', file='learn_export_throwin.pkl')
        self.ti_model.to_fp32()
        print(self.ti_model.data.classes)

        print('Loading video...')
        self.cap = cv2.VideoCapture(vid_path)
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, int(self.fps * 60 * self.start_time))

        cv2.namedWindow('Video', cv2.WINDOW_GUI_NORMAL)
        cv2.resizeWindow('Video', 720, 400)
        if show_output:
            print('Loading UI...')
            # Refactoring needed
            # Initiate 1st Qt window
            self.app = QtWidgets.QApplication(sys.argv)
            self.MainWindow_event_detection = QtWidgets.QMainWindow()
            self.ui_event_detection = interface.Ui_MainWindow()
            self.ui_event_detection.setupUi(self.MainWindow_event_detection)
            # self.ui_event_detection.bet.hide()
            self.ui_event_detection.bet.setText('Replay')
            self.ui_event_detection.bet.setStyleSheet("background-color: gray")
            self.MainWindow_event_detection.setWindowTitle("Event Detection")
            self.MainWindow_event_detection.show()

            # Initiate 2nd Qt window
            self.MainWindow_forecasting = QtWidgets.QMainWindow()
            self.ui_forecasting = interface.Ui_MainWindow()
            self.ui_forecasting.setupUi(self.MainWindow_forecasting)
            self.MainWindow_forecasting.setWindowTitle("Forecasting")
            self.MainWindow_forecasting.show()
            # self.x = threading.Thread(target=self.process)
            # self.x.start()
            self.start()
            self.app.exec()
        else:
            self.process()

    def process(self):
        print('Performing inference...')
        time.sleep(1)
        q = deque(maxlen=32)
        preds = []
        preds_sb = []
        preds_camview = []
        preds_ti = []
        frames_all = []
        gen_filter = np.zeros(6 * 1).reshape(6, 1)
        last_event_detection_timestamp = np.zeros(6 * 1).reshape(6, 1)
        last_event_forecasting_timestamp = np.zeros(6 * 1).reshape(6, 1)
        pred_rate_frames = int(self.fps * self.pred_rate)
        old_replay = False
        old_bet_stop = False
        try:
            while True:
                ret, frame = self.cap.read()
                if not ret: break
                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, c = frame.shape
                l_buff = w // self.l_buff_div
                frame_sb = frame[:h // 2, l_buff:l_buff + h // 2]
                frame_sb_rgb = cv2.cvtColor(frame_sb, cv2.COLOR_BGR2RGB)
                frame_pos = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES))
                msec = self.cap.get(cv2.CAP_PROP_POS_MSEC)
                q.append(frame)
                self.frame_num.append(frame_pos)
                if len(q) > self.skip_frames * self.past_frames and frame_pos % 1 == 0:
                    event = get_predict_event_chunk(q, skip_frames=self.skip_frames, past_frames=self.past_frames)
                    with self.graph.as_default():
                        pred = self.model.predict(np.expand_dims(event, 0))
                    pred = np.reshape(pred, (7, 1))

                    # Predict camera view
                    if frame_pos % 1 == 0 or 'pred_camview' not in locals():
                        img_torch = fastaiImage(pil2tensor(Image.fromarray(frame_rgb), np.float32).div(255))
                        pred_camview = self.camview_model.predict(img_torch)
                        pred_camview = np.array(list(pred_camview[2]))

                        pred_ti = self.ti_model.predict(img_torch)
                        pred_ti = np.array(list(pred_ti[2]))

                    # Predict replay
                    if frame_pos % 1 == 0 or 'replay' not in locals():
                        img_torch = fastaiImage(pil2tensor(
                            Image.fromarray(frame_sb_rgb), np.float32).div(255))
                        pred_sb = self.sb_model.predict(img_torch)
                        pred_sb = np.array(list(pred_sb[2]))
                        replay = pred_sb[0] > 0.5
                        if self.show_output:
                            if old_replay != replay:
                                old_replay = replay
                                if replay:
                                    self.ui_event_detection.bet.setStyleSheet("background-color: red")
                                else:
                                    self.ui_event_detection.bet.setStyleSheet("background-color: gray")

                    preds.append(pred)
                    preds_sb.append(pred_sb)
                    preds_camview.append(pred_camview)
                    preds_ti.append(pred_ti)
                    frames_all.append(frame_pos)

                    pred = pred.reshape((1, 7))
                    pred_camview = pred_camview.reshape((1, 9))
                    # final_input = np.concatenate((pred, pred_camview), axis=1)
                    # final_pred = self.model_filter.predict(final_input)

                    pred[0, 6] += pred_camview[0, 8] * 5
                    pred[0, 3] += pred_ti[1]

                    gen_filter = self.filter1(gen_filter, pred)
                    self.frame_pred.append(np.array(pred))

                    seconds = int((msec / 1000) % 60)
                    minutes = int((msec / (1000 * 60)))
                    print(f'time: {minutes}:{seconds}  label: {self.labels} {np.round(gen_filter.reshape(1, 6), 2)}')

                    if self.show_output:
                        if not replay:
                            for i in range(0, len(gen_filter)):
                                if gen_filter[i] >= self.threshold_list_event_detection[i]:
                                    if msec - last_event_detection_timestamp[i] > 10000:
                                        last_event_detection_timestamp[i] = msec
                                        data_to_paste = f'time: {minutes:02d}:{seconds:02d} event: {self.labels[i]} {np.round(gen_filter[i], 2)}'
                                        self.ui_event_detection.data_feed.append(data_to_paste)
                                        self.button_red(self.ui_labels[i], self.ui_event_detection)

                                if gen_filter[i] >= self.threshold_list_forecasting[i]:
                                    if msec - last_event_forecasting_timestamp[i] > 2000:
                                        last_event_forecasting_timestamp[i] = msec
                                        data_to_paste = f'time: {minutes:02d}:{seconds:02d} event: {self.labels[i]} {np.round(gen_filter[i], 2)}'
                                        self.ui_forecasting.data_feed.append(data_to_paste)
                                        self.button_red(self.ui_labels[i], self.ui_forecasting)

                            bet_stop = (gen_filter[1] >= self.threshold_list_forecasting[1])or\
                                       (gen_filter[3] >= self.threshold_list_forecasting[3])or\
                                       (gen_filter[4] >= self.threshold_list_forecasting[4])or\
                                       (gen_filter[5] >= self.threshold_list_forecasting[5])
                            if bet_stop != old_bet_stop:
                                old_bet_stop = bet_stop
                                if bet_stop:
                                    self.ui_forecasting.bet.setStyleSheet("background-color: red")
                                else:
                                    self.ui_forecasting.bet.setStyleSheet("background-color: green")
                        QtTest.QTest.qWait(5)

                if self.show_cv:
                    cv2.imshow('Video', frame)
                    cv2.waitKey(5)
        except:
            print(traceback.print_exc())
        with open('resources/match.pickle', 'wb') as f:
            pickle.dump((preds, preds_camview, preds_sb, preds_ti, frames_all), f)
        if self.write_csv:
            self.write_results()

    def filter1(self, gen_filter, pred):
        gen_filter = np.dot(self.a, gen_filter) + np.dot(self.b2, pred.reshape(7,1))
        return gen_filter

    def filter2(self, gen_filter, pred):
        gen_filter = np.dot(self.a, gen_filter) + np.dot(self.b, pred[0, 1:].reshape((6, 1)))
        return gen_filter

    def grayout_buttons(self, ui):
        time.sleep(2)
        for l in self.ui_labels[1:]:
            # print(f'gray out {l}')
            getattr(ui, l).setStyleSheet("background-color: gray")

    def button_red(self, label, ui):
        if label in self.ui_labels[1:]:
            getattr(ui, label).setStyleSheet("background-color: red")
            t = threading.Thread(target=self.grayout_buttons, args=(ui,)).start()

    def destroy(self):
        self.cap.release()
        if self.show_output:
            cv2.destroyAllWindows()

    def write_results(self):
        pred_model_final = np.zeros(len(self.frame_num) * 8).reshape(len(self.frame_num), 8)
        pred_model_final[:, 0] = self.frame_num
        frame_pred = np.array(self.frame_pred)
        frame_pred = frame_pred.reshape(frame_pred.shape[0], 7)

        pred_model_final[self.skip_frames * self.past_frames:, 1:8] = frame_pred[:, :]

        df = pd.DataFrame(data=pred_model_final[:, :])
        df.to_csv(f'models/{self.model_name}/pred_TEST_MATCH_compressed.csv', index=False)

    def run(self):
        self.process()


if __name__ == '__main__':
    infer = Infer(param_file='params.yaml', show_output=False, show_cv=False,
                  write_csv=False)
