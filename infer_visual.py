import cv2
import sys
import pickle
import threading
import numpy as np
import yaml
import time
from collections import deque
from configparser import ConfigParser
import traceback
from tools import interface
from PyQt5 import QtGui, QtWidgets, QtCore, QtTest


class Infer(QtCore.QThread):
    def __init__(self, param_file='params.yaml', show_output=True,
                 show_cv=False, write_csv=False):
        QtCore.QThread.__init__(self)
        self.show_output = show_output
        self.show_cv = show_cv
        self.write_csv = write_csv
        with open(param_file, 'r') as stream:
            config = yaml.safe_load(stream)
        self.model_name = config['inference']['model_name']
        self.l_buff_div = config['inference']['sb']['l_buff_div']
        self.start_time = config['inference']['start_time']
        vid_path = config['inference']['vid_path']
        self.pred_rate = config['inference']['pred_rate']
        a_list = np.array(config['inference']['a'])
        b_list = np.array(config['inference']['b'])

        model_config = ConfigParser()
        model_config.read(f'models/{self.model_name}/params.ini')
        self.past_frames = int(model_config.get('general', 'past_frames'))
        self.skip_frames = int(model_config.get('general', 'skip_frames'))
        self.labels = np.array(config['inference']['labels'])
        self.ui_labels = ['', 'corner', 'throw_in', 'shot', 'goal', 'yellow_card']
        print(self.labels)
        self.threshold_list_event_detection = np.array(config['inference']['thr'])
        self.threshold_list_forecasting = np.array(config['inference']['thr_pred'])
        self.a = np.diag(a_list)
        self.b = np.eye(6) - self.a
        self.b2 = np.concatenate((-b_list.reshape(6,1), self.b), axis=1)
        self.frame_num = []
        self.frame_pred = []
        self.threshold_list_event_detection = np.array(config['inference']['thr'])
        self.threshold_list_forecasting = np.array(config['inference']['thr_pred'])
        self.threads = []

        print('Loading video...')
        self.cap = cv2.VideoCapture(vid_path)
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, int(self.fps * 60 * self.start_time))
        self.lock = False

        cv2.namedWindow('Video', cv2.WINDOW_GUI_NORMAL)
        cv2.resizeWindow('Video', 720, 400)

        if show_output:
            print('Loading UI...')
            # Refactoring needed
            # Initiate 1st Qt window
            self.app = QtWidgets.QApplication(sys.argv)
            self.MainWindow_event_detection = QtWidgets.QMainWindow()
            self.ui_event_detection = interface.Ui_MainWindow()
            self.ui_event_detection.setupUi(self.MainWindow_event_detection)
            # self.ui_event_detection.bet.hide()
            self.ui_event_detection.bet.setText('Replay')
            self.ui_event_detection.bet.setStyleSheet("background-color: gray")
            self.MainWindow_event_detection.setWindowTitle("Event Detection")
            self.MainWindow_event_detection.show()

            # Initiate 2nd Qt window
            self.MainWindow_forecasting = QtWidgets.QMainWindow()
            self.ui_forecasting = interface.Ui_MainWindow()
            self.ui_forecasting.setupUi(self.MainWindow_forecasting)
            self.MainWindow_forecasting.setWindowTitle("Forecasting")
            self.MainWindow_forecasting.show()
            # self.x = threading.Thread(target=self.process)
            # self.x.start()
            self.start()
            self.app.exec()
        else:
            self.process()

    def process(self):
        print('Performing inference...')
        time.sleep(1)
        q = deque(maxlen=32)
        gen_filter = np.zeros(6 * 1).reshape(6, 1)
        last_event_detection_timestamp = np.zeros(6 * 1).reshape(6, 1)
        last_event_forecasting_timestamp = np.zeros(6 * 1).reshape(6, 1)
        pred_rate_frames = int(self.fps * self.pred_rate)
        old_replay = False
        old_bet_stop = False
        with open('resources/match.pickle', 'rb') as f:
            (preds, preds_camview, preds_sb, preds_ti, frames_all) = pickle.load(f)
        try:
            while True:
                ret, frame = self.cap.read()
                if not ret: break
                frame_pos = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES))
                msec = self.cap.get(cv2.CAP_PROP_POS_MSEC)
                q.append(frame)
                self.frame_num.append(frame_pos)
                if len(q) > self.skip_frames * self.past_frames and frame_pos % 1 == 0:
                    idx = np.where(np.array(frames_all) == frame_pos)[0][0]
                    pred = preds[idx]
                    pred_sb = preds_sb[idx]
                    pred_camview = preds_camview[idx]
                    pred_ti = preds_ti[idx]

                    pred = pred.reshape((1, 7))
                    pred_camview = pred_camview.reshape((1, 9))

                    replay = pred_sb[0] > 0.4
                    if self.show_output:
                        if old_replay != replay:
                            old_replay = replay
                            if replay:
                                self.ui_event_detection.bet.setStyleSheet("background-color: red")
                            else:
                                self.ui_event_detection.bet.setStyleSheet("background-color: gray")
                    # final_input = np.concatenate((pred, pred_camview), axis=1)
                    # final_pred = self.model_filter.predict(final_input)

                    pred[0, 6] += pred_camview[0, 8] * 5
                    pred[0, 3] += pred_ti[1]

                    gen_filter = self.filter1(gen_filter, pred)
                    self.frame_pred.append(np.array(pred))

                    seconds = int((msec / 1000) % 60)
                    minutes = int((msec / (1000 * 60)))
                    print(f'time: {minutes}:{seconds}  label: {self.labels} {np.round(gen_filter.reshape(1, 6), 2)}')

                    if self.show_output:
                        if not replay:
                            for i in range(0, len(gen_filter)):
                                if gen_filter[i] >= self.threshold_list_event_detection[i]:
                                    if msec - last_event_detection_timestamp[i] > 10000:
                                        last_event_detection_timestamp[i] = msec
                                        data_to_paste = f'time: {minutes:02d}:{seconds:02d} event: {self.labels[i]} {np.round(gen_filter[i], 2)}'
                                        self.ui_event_detection.data_feed.append(data_to_paste)
                                        self.button_red(self.ui_labels[i], self.ui_event_detection)

                                if gen_filter[i] >= self.threshold_list_forecasting[i]:
                                    if msec - last_event_forecasting_timestamp[i] > 2000:
                                        last_event_forecasting_timestamp[i] = msec
                                        data_to_paste = f'time: {minutes:02d}:{seconds:02d} event: {self.labels[i]} {np.round(gen_filter[i], 2)}'
                                        self.ui_forecasting.data_feed.append(data_to_paste)
                                        self.button_red(self.ui_labels[i], self.ui_forecasting)

                            bet_stop = (gen_filter[1] >= self.threshold_list_forecasting[1])or\
                                       (gen_filter[3] >= self.threshold_list_forecasting[3])or\
                                       (gen_filter[4] >= self.threshold_list_forecasting[4])or\
                                       (gen_filter[5] >= self.threshold_list_forecasting[5])
                            if bet_stop != old_bet_stop:
                                old_bet_stop = bet_stop
                                if bet_stop:
                                    self.ui_forecasting.bet.setStyleSheet("background-color: red")
                                else:
                                    self.ui_forecasting.bet.setStyleSheet("background-color: green")
                        QtTest.QTest.qWait(10)

                if self.show_cv:
                    cv2.imshow('Video', frame)
                    cv2.waitKey(50)
        except:
            print(traceback.print_exc())

    def filter1(self, gen_filter, pred):
        gen_filter = np.dot(self.a, gen_filter) + np.dot(self.b2, pred.reshape(7,1))
        return gen_filter

    def filter2(self, gen_filter, pred):
        gen_filter = np.dot(self.a, gen_filter) + np.dot(self.b, pred[0, 1:].reshape((6, 1)))
        return gen_filter

    def grayout_buttons(self, ui):
        time.sleep(2)
        while self.lock:
            QtTest.QTest.qWait(5)
        self.lock = True
        for l in self.ui_labels[1:]:
            getattr(ui, l).setStyleSheet("background-color: gray")
        self.lock = False

    def button_red(self, label, ui):
        while self.lock:
            QtTest.QTest.qWait(5)
        self.lock = True
        if label in self.ui_labels[1:]:
            getattr(ui, label).setStyleSheet("background-color: red")
            t = threading.Thread(target=self.grayout_buttons, args=(ui,))
            t.start()
            self.threads.append(t)
        self.lock = False

    def destroy(self):
        self.cap.release()
        if self.show_output:
            cv2.destroyAllWindows()

    def run(self):
        self.process()


if __name__ == '__main__':
    if len(sys.argv) == 2:
        yaml_file = sys.argv[1]
    else:
        yaml_file = 'params.yaml'
    infer = Infer(param_file=yaml_file, show_output=True, show_cv=True,
                  write_csv=False)
