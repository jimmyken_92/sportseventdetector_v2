import os
import ast
import configparser
from shutil import copyfile
from pathlib import Path
from datetime import datetime
import numpy as np
import pandas as pd
import pickle
from keras.models import model_from_json, load_model
from tools.layer_tools import get_inceptionv3, get_resnet50, unfreeze
from data_generator import DataGenerator, split_train_val_test
from tools.performance_tools import get_full_scores, plot_roc_auc, plot_confusion_matrix, \
plot_roc_auc_per_class, concatenate_plots, threshold_selector
import matplotlib.pyplot as plt
from sklearn.metrics import multilabel_confusion_matrix
from keras.callbacks import LearningRateScheduler, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.models import load_model
from keras.optimizers import Adam


class ModelTrainer:
    def __init__(self, config_file, try_folder=None):
        np.random.seed(88)
        if try_folder is None:
            self.try_folder = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
            new_path = Path(f'models/{self.try_folder}')
            if not new_path.exists():
                new_path.mkdir()
        else:
            self.try_folder = try_folder
            config_file = f'models/{try_folder}/params.ini'

        self.train_generator = None
        self.val_generator = None
        self.test_generator = None
        self.model = None

        self.config_file = config_file
        cp = configparser.ConfigParser()
        cp.read(self.config_file)

        self.model_name = cp.get('general', 'model_name')
        self.past_frames = int(cp.get('general', 'past_frames'))
        self.target_names = ast.literal_eval(cp.get('general', 'target_names'))
        self.train_params = ast.literal_eval(cp.get('general', 'train_params'))
        self.compile_params = eval(cp.get('general', 'compile_params'))
        self.val_pc = float(cp.get('general', 'val_pc'))
        self.test_matches = ast.literal_eval(cp.get('general', 'test_matches'))
        self.dataset = cp.get('general', 'dataset')
        self.batch_size = int(cp.get('general', 'batch_size'))
        self.norm_factor = float(cp.get('general', 'norm_factor'))
        self.skip_frames = int(cp.get('general', 'skip_frames'))
        self.full_event_skip = int(cp.get('general', 'full_event_skip'))
        self.compile_params_after_unfreeze = eval(cp.get('general','compile_params_after_unfreeze'))
        self.params_after_unfreeze = ast.literal_eval(cp.get('general', 'params_after_unfreeze'))
        self.use_preprocessor = ast.literal_eval(cp.get('general', 'use_preprocessor'))
        self.lr_patience = int(cp.get('callback_params', 'lr_patience'))
        self.lr_factor = float(cp.get('callback_params', 'lr_factor'))
        self.early_stopping_patience = int(cp.get('callback_params', 'early_stopping_patience'))
        self.unfreeze_lr_patience = int(cp.get('callback_params', 'unfreeze_patience'))
        self.unfreeze_lr_factor = float(cp.get('callback_params', 'unfreeze_factor'))
        self.unfreeze_early_stopping_patience = int(cp.get('callback_params', 'unfreeze_stopping_patience'))

    def process(self):
        self._create_model()
        self._create_generators()
        self._train()
        self._save_model_and_files()
        self._test()

    def test_from_folder(self):
        if self.model is None:
            self.model = load_model(f'models/{self.try_folder}/model.h5')
        self._create_generators()
        self._test()

    def _create_model(self):
        if self.model_name == 'resnet50':
            self.model = get_resnet50(self.past_frames, self.target_names.__len__())
            self.model.name = self.model_name
        elif self.model_name == 'inception':
            self.model = get_inceptionv3(self.past_frames, self.target_names.__len__())
            self.model.name = self.model_name
        else:
            self.model = model_from_json(open(f'models/{self.model_name}.json', 'r').read())
        self.model.compile(**self.compile_params)

    def _create_generators(self):
        if self.model.layers[0].input.shape.__len__() == 5:
            use_color = True if self.model.layers[0].input.shape[4].value == 3 else False
            sz = (self.model.layers[0].input.shape[2].value, self.model.layers[0].input.shape[3].value)
            use_sequential_frames = False
        else:
            use_color = False
            sz = (224, 224)
            use_sequential_frames = True

        event_dirs = os.listdir(self.dataset)
        event_dirs.sort()
        train_evs, val_evs, test_evs = split_train_val_test(event_dirs, self.test_matches, self.val_pc)
        params = {'batch_size': self.batch_size, 'use_color': use_color, 'dim': sz, 'past_frames': self.past_frames,
                  'sequential_images': use_sequential_frames, 'dataset': self.dataset, 'normalizer': self.norm_factor,
                  'skip': self.skip_frames, 'full_event_skip': self.full_event_skip,
                  'use_preprocessor': self.use_preprocessor}
        # print(train_evs)
        self.train_generator = DataGenerator(train_evs, **params)
        self.val_generator = DataGenerator(val_evs, **params)
        self.test_generator = DataGenerator(test_evs, **params)

    def _train(self):
        callbacks = [
            EarlyStopping(monitor='val_loss', patience=self.early_stopping_patience, mode='min'),
            ModelCheckpoint(filepath= f'models/{self.try_folder}/model_earl_stop.h5', monitor='val_loss', mode='min',
                            save_best_only=True, verbose=0),
            ReduceLROnPlateau(monitor='val_loss', factor=self.lr_factor, patience=self.lr_patience, min_lr=0, verbose=1)
        ]
        print(f'callbacks: {callbacks}')
        try:
            history1 = self.model.fit_generator(self.train_generator, validation_data=self.val_generator,
                                                **self.train_params, callbacks=callbacks)
            print('Train ended')
            val_loss = history1.history['val_loss']
            np.savetxt(f"models/{self.try_folder}/val_loss.csv", val_loss, delimiter=",")
        except KeyboardInterrupt:
            print('\n Break detected. Wait to test the model.\n')

        if self.params_after_unfreeze is not None:
            unfreeze_callbacks = [
                EarlyStopping(monitor='val_loss', patience=self.unfreeze_early_stopping_patience, mode='min'),
                ModelCheckpoint(filepath=f'models/{self.try_folder}/model.h5', monitor='val_loss', mode='min',
                                save_best_only=True, verbose=0),
                ReduceLROnPlateau(monitor='val_loss', factor=self.unfreeze_lr_factor, patience=self.unfreeze_lr_patience,
                                  min_lr=0,verbose=1)
            ]
            print('Unfreeze and retrain')
            self.model = load_model(f'models/{self.try_folder}/model_earl_stop.h5')
            unfreeze(self.model)
            self.model.compile(**self.compile_params_after_unfreeze)
            try:
                history2 = self.model.fit_generator(self.train_generator, validation_data=self.val_generator,
                                         **self.params_after_unfreeze, callbacks=unfreeze_callbacks)
                print('Train Finished')
                unfreeze_val_loss = history2.history['val_loss']
                np.savetxt(f"models/{self.try_folder}/unfreeze_val_loss.csv", unfreeze_val_loss, delimiter=",")

                all_val_loss = val_loss + unfreeze_val_loss
                temp = []
                for i in range(0,len(val_loss)):
                    temp.append(i)
                unfreeze_temp = []
                for i in range(0,len(unfreeze_val_loss)):
                    unfreeze_temp.append(i+len(val_loss))
                all_epochs = temp + unfreeze_temp
                all_epochs = np.array(all_epochs)
                final_array = np.zeros(len(all_val_loss) * 2).reshape(len(all_val_loss), 2)
                final_array[:, 0] = all_val_loss
                final_array[:, 1] = all_epochs
                df = pd.DataFrame(data=final_array, columns=['val_loss', 'epoch'])
                df.to_csv(f"models/{self.try_folder}/all_val_loss.csv", index=False, sep=',')
                fig = plt.figure(figsize=(10, 8))
                plt.plot(df.values[:, 1], df.values[:, 0])
                plt.title('model loss')
                plt.ylabel('loss')
                plt.xlabel('epoch')
                plt.legend(['validation_loss'], loc='upper left')
                plt.grid()
                plt.savefig(f"models/{self.try_folder}/loss_plot.png")
                plt.close()

            except KeyboardInterrupt:
                print('\n Break detected. Wait to test the model.\n')

    def _save_model_and_files(self):
        #self.model.save(f'models/{self.try_folder}/model.h5')
        try:
            copyfile(f'models/{self.model_name}.json', f'models/{self.try_folder}/model.json')
        except FileNotFoundError:
            print('no model file')
        copyfile(self.config_file, f'models/{self.try_folder}/params.ini')
        copyfile('data_generator.py', f'models/{self.try_folder}/data_generator.py')
        copyfile('keras_trainer.py', f'models/{self.try_folder}/keras_trainer.py')
        with open(f'models/{self.try_folder}/summary.txt', 'w') as f:
            self.model.summary(line_length=80, print_fn=f.write)

    def _test(self):
        print('Validating training set')
        train_score = get_full_scores(self.model, self.train_generator)
        # we ask from plot_roc_auc to plot the roc curves for all the classes only for training set
        plot_roc_auc(y_score=train_score, y_test=self.train_generator.labels, labels=self.target_names,
                     filename=f'models/{self.try_folder}/ROC_AUC_train.jpg')
        print('Validating validation set')
        # we ask from plot_roc_auc to plot the roc curves for all the classes only for validation set
        val_score = get_full_scores(self.model, self.val_generator)
        plot_roc_auc(y_score=val_score, y_test=self.val_generator.labels, labels=self.target_names,
                     filename=f'models/{self.try_folder}/ROC_AUC_validation.jpg')
        print('Validating test set')
        # we ask from plot_roc_auc to plot the roc curves for all the classes only for test set
        test_score = get_full_scores(self.model, self.test_generator)
        plot_roc_auc(y_score=test_score, y_test=self.test_generator.labels, labels=self.target_names,
                     filename=f'models/{self.try_folder}/ROC_AUC_test.jpg')

        # concatenate the three plots(training,validation,test) that contain all classes and save them to single .jpg file
        concatenate_plots(self.try_folder, 3, self.target_names)


        for i in range(0, len(self.target_names)):
            # for all classes: plot train/validation/test set curves in one one figure
            # this results to n(no_of_classes) number of jpg files
            plot_roc_auc_per_class(self.train_generator.labels, train_score,
                                   self.val_generator.labels, val_score,
                                   self.test_generator.labels, test_score,
                                   self.target_names, i, f'models/{self.try_folder}/{self.target_names[i]}_ROC_AUC.jpg')

        y_true = np.array(self.test_generator.labels)
        test_score = np.array(test_score)

        # automated procedure for picking the optimal threshold values for each class
        opt_thresh = np.zeros(len(self.target_names)*1).reshape(len(self.target_names),1)
        for i in range(0,len(self.target_names)):
            opt_thresh[i] = threshold_selector(self.test_generator.labels[:, i], test_score[:, i], self.target_names[i])
            print("Optimal threshold for class",self.target_names[i],":",opt_thresh[i])

        for i in range(0,len(opt_thresh)):
            with open(f'models/{self.try_folder}/opt_thr_%i.pickle' %(i), 'wb') as f:
                pickle.dump(opt_thresh[i], f)

        y_pred = np.zeros(test_score.shape[0]*len(self.target_names)).reshape(test_score.shape[0],len(self.target_names))
        for i in range(0,len(self.target_names)):
            y_pred[:,i] = test_score[:,i] >= opt_thresh[i]


        print('Confusion Matrices')
        # plot n(no_of_classes) multilabel confusion matrices with one vs rest method
        a = multilabel_confusion_matrix(y_true, y_pred)
        for i in range(0, len(self.target_names)):
            current_target_names = ['rest', self.target_names[i]]
            plot_confusion_matrix(a[i], classes=current_target_names, normalize=False)
            plt.savefig(f'models/{self.try_folder}/{self.target_names[i]}_vs_rest.jpg')

        # plot n(no_of_classes) multilabel normalized confusion matrices with one vs rest method
        a = multilabel_confusion_matrix(y_true, y_pred)
        for i in range(0, len(self.target_names)):
            current_target_names = ['rest', self.target_names[i], ]
            plot_confusion_matrix(a[i], classes=current_target_names, normalize=True)
            plt.savefig(f'models/{self.try_folder}/{self.target_names[i]}_vs_rest_normalized.jpg')
