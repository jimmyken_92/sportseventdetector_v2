import os; os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import argparse
import sys


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--create_ds", help="Construct dataset", action='store_true')
    parser.add_argument("-d", "--delete_ds", help="Delete dataset related to config file", action='store_true')
    parser.add_argument("-o", "--create_only_ds", help="Only construct dataset end exit", action='store_true')
    parser.add_argument("-f", "--config_file", help="Define .ini file")
    parser.add_argument('-l', '--list_config_files', nargs='+', help='List of .ini files')
    parser.add_argument("-t", "--test_folder", help="Test model .h5 in defined TEST_FOLDER")
    args = parser.parse_args()

    from keras_trainer import ModelTrainer
    from dataset_creator import create_dataset, delete_dataset

    if not args.test_folder:
        if args.config_file:
            configuration_files = [args.config_file]
        elif args.list_config_files:
            configuration_files = args.list_config_files
        else:
            configuration_files = ['params.ini']
        for config in configuration_files:
            if args.create_ds or args.create_only_ds:
                create_dataset(config)
                if args.create_only_ds:
                    sys.exit(0)
            m = ModelTrainer(config)
            m.process()
            if args.delete_ds:
                delete_dataset(config)
    else:
        m = ModelTrainer(None, try_folder=args.test_folder)
        m.test_from_folder()
