import pandas as pd
import os
import cv2
from pathlib import Path
import numpy as np


os.chdir("data_raw")
files = os.listdir(os.curdir)

for i in range(2,len(files)):
    df = pd.read_csv(f"{files[i]}/{files[i]}_SHOT.csv")

    start_point_timestamps = []
    end_point_timestamps = []
    shot_timestamps = []
    no_event_timestamps = []


    for j in range(0,df.shape[0]):
        if(df.iloc[j,0][:5]=="Point"):
            start_point_timestamps.append(df.iloc[j,1])
            end_point_timestamps.append(df.iloc[j,1]+df.iloc[j,2])
            no_event_timestamps.append((df.iloc[j,1]+df.iloc[j,2]+start_point_timestamps[-1])/2)
        if(df.iloc[j,0][:4]=="Shot"):
            shot_timestamps.append(df.iloc[j,1])

    os.chdir("..")
    dataset = Path.cwd()/"dataset"
    print(os.getcwd())
    video_file = f'data_raw/{files[i]}/{files[i]}.mp4'
    cap = cv2.VideoCapture(str(video_file))
    fps = int(cap.get(cv2.CAP_PROP_FPS))

    # print(fps)

    classes = [0,1,2,3]
    for k in range(0,len(classes)):
        if (k==1):
            for l in range(0,len(start_point_timestamps)):
                event_folder = dataset / f"[{k}]_{l}_{files[i]}"
                Path(event_folder).mkdir(parents=True, exist_ok=True)  # Make 'events' dir
                timestamp = start_point_timestamps[l]
                timestamp = timestamp /1000
                cap.set(cv2.CAP_PROP_POS_MSEC, (timestamp - int(3 * 2 / 3)) * 1000)
                for m in range(3 * fps):
                    ret, frame = cap.read()
                    frame = cv2.resize(frame, (480, 270))
                    cv2.imwrite(str(event_folder / f'{m}.jpg'), frame)
        elif(k==2):
            for l in range(0,len(end_point_timestamps)):
                event_folder = dataset / f"[{k}]_{l}_{files[i]}"
                Path(event_folder).mkdir(parents=True, exist_ok=True)  # Make 'events' dir
                timestamp = end_point_timestamps[l]
                timestamp = timestamp /1000
                cap.set(cv2.CAP_PROP_POS_MSEC, (timestamp - int(3 * 2 / 3)) * 1000)
                for m in range(3 * fps):
                    ret, frame = cap.read()
                    frame = cv2.resize(frame, (480, 270))
                    cv2.imwrite(str(event_folder / f'{m}.jpg'), frame)
        elif(k==3):
            for l in range(0,len(shot_timestamps)):
                event_folder = dataset / f"[{k}]_{l}_{files[i]}"
                Path(event_folder).mkdir(parents=True, exist_ok=True)  # Make 'events' dir
                timestamp = shot_timestamps[l]
                timestamp = timestamp /1000
                cap.set(cv2.CAP_PROP_POS_MSEC, (timestamp - int(3 * 2 / 3)) * 1000)
                for m in range(3 * fps):
                    ret, frame = cap.read()
                    frame = cv2.resize(frame, (480, 270))
                    cv2.imwrite(str(event_folder / f'{m}.jpg'), frame)


        if(k==0):
            for l in range(0,len(no_event_timestamps)):
                event_folder = dataset / f"[{k}]_{l}_{files[i]}"
                Path(event_folder).mkdir(parents=True, exist_ok=True)  # Make 'events' dir
                timestamp = no_event_timestamps[l]
                timestamp = timestamp /1000
                cap.set(cv2.CAP_PROP_POS_MSEC, (timestamp - int(3 * 2 / 3)) * 1000)
                for m in range(3 * fps):
                    ret, frame = cap.read()
                    frame = cv2.resize(frame, (480, 270))
                    cv2.imwrite(str(event_folder / f'{m}.jpg'), frame)
    os.chdir("data_raw")
