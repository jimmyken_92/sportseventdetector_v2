import numpy as np
import cv2
import yaml
from fastai.vision import load_learner, pil2tensor, Image
import PIL
from pathlib import Path

with open('params.yaml', 'r') as stream:
    config = yaml.safe_load(stream)
model_dir = Path(config['inference_cam_view']['model_dir'])
vid_name = Path(config['inference_cam_view']['vid_name'])
data_dir = Path(config['inference_cam_view']['data_dir'])
pred_rate_frames = config['inference_cam_view']['pred_rate_frames']

# Read video
# ----------
# Start video capture
cap = cv2.VideoCapture(str(data_dir/vid_name))
ret, frame = cap.read()
frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
assert total_frames, 'There are no frames in the video path'
fps = cap.get(cv2.CAP_PROP_FPS)
frame_pos = int(cap.get(cv2.CAP_PROP_POS_FRAMES))

cap.set(cv2.CAP_PROP_POS_FRAMES, fps*60*40)

print('Loading learner...')
learn = load_learner(model_dir, file='learn_export_camview.pkl')
learn.to_fp32()

frame_pos_all = []
preds = []
while True:
    ret, frame = cap.read()
    if not ret: break
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    height, width, channel = frame.shape
    frame_pos = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    if not frame_pos % pred_rate_frames == 0:
        continue

    img_torch = Image(pil2tensor(PIL.Image.fromarray(frame_rgb),
                                 np.float32).div(255))
    pred = learn.predict(img_torch)
    preds.append(list(pred[2].numpy()))
    frame_pos_all.append(frame_pos)
    print(frame_pos, [round(i) for i in pred[2].numpy()], pred[0].obj)
    cv2.imshow('orig', frame)
    cv2.waitKey(0)

cap.release()
cv2.destroyAllWindows()

# Save
classes = learn.data.classes
with open(data_dir/'classes.txt', 'w', newline='') as f:
    for c in classes:
        f.write(f'{c}\n')


csv_out = []
for i in range(len(frame_pos_all)):
    csv_out.append([frame_pos_all[i]] + [j for j in preds[i]])
np.savetxt(data_dir/'cam_view_preds.csv', np.array(csv_out), fmt='%1.3f', delimiter=',')
