import fastai.vision as fa
import matplotlib.pyplot as plt
import numpy as np
from keras.utils import to_categorical
import os
import cv2
from typing import List
import random
from os import listdir
from os.path import isfile, join
from tools.experimantal.football_conv_lstm import split_train_val_test
import ast

DATASET = 'dataset1/'


class DataGenerator(fa.Dataset):
    class Dummy:
        def __init__(self, classes):
            self.classes = classes

    def __init__(self, list_paths: List, classes: List, batch_size: int = 32, dim: tuple = (224, 224), shuffle: bool = True,
                 skip: int = 15, past_frames: int = 3, use_color: bool = False):
        self.dim = dim
        self.batch_size = batch_size
        self.list_paths = list_paths
        self.shuffle = shuffle
        self.skip = skip
        self.past_frames = past_frames
        self.labels = []
        self.use_color = use_color
        self.classes = classes
        self.c = classes.__len__()

        self.on_epoch_end()
        # self.y = self.Dummy(classes)
        # self.y = fa.MultiCategoryList()

    def get_full_event(self, event_id):
        full_event = []
        event_name = self.list_paths[event_id]
        images = [f for f in listdir(DATASET + event_name) if isfile(join(DATASET + event_name, f))]
        for start_frame in range(images.__len__() - self.past_frames * self.skip - 1):
            event = self.get_event_chunk(event_name, start_frame)
            event = np.stack(event, axis=0)
            full_event.append(event)
        full_event = np.stack(full_event, axis=0)
        if not self.use_color:
            full_event = np.expand_dims(full_event, axis=4)
        return full_event

    def get_event_chunk(self, path, start_frame):
        event = []
        for i in range(start_frame, start_frame + self.skip * self.past_frames, self.skip):
            if not self.use_color:
                im = cv2.imread(DATASET + path + f'/{i}.jpg', 0)
            else:
                im = cv2.imread(DATASET + path + f'/{i}.jpg')
            im = cv2.resize(im, self.dim, None)
            event.append(im)
        return event

    def __len__(self):
        return len(self.list_paths)

    def __getitem__(self, index):
        path = self.list_paths[index]
        y = self.labels[index]

        # yy=fa.MultiCategory(y, np.where(y==1.0), np.where(y==1.0))
        number_of_ims = os.listdir(DATASET + path).__len__() - 1
        start_frame = np.random.randint(number_of_ims - self.skip * self.past_frames)
        x = self.get_event_chunk(path, start_frame)
        x = np.array(x, dtype='float32')
        # x=x/255
        return x, y

    def on_epoch_end(self):
        random.shuffle(self.list_paths)
        labels = []
        for path in self.list_paths:
            # number_of_ims = os.listdir(DATASET + path).__len__() - 1
            # print(f'{number_of_ims}  {path}')
            labels.append(ast.literal_eval(path.split('_')[0]))
        if type(labels[0]) is not list:
            self.labels = to_categorical(labels)
            idx = np.argwhere(np.all(self.labels[..., :] == 0, axis=0))
            self.labels = np.delete(self.labels, idx, axis=1)
        else:
            str_labels = []
            a={30:self.classes[0], 36:self.classes[1], 100:self.classes[2]}
            for label in labels:
                temp=''
                for l in label:
                    temp += a[l] + ' '
                str_labels.append(temp[:-1])
            self.labels = fa.MultiCategoryList(np.array(str_labels), label_delim=' ')
            self.y = self.labels
            self.processor=fa.MultiCategoryProcessor(self)
            self.processor.process(self.y)

    def to_multicalss_categorical(self, labels):
        target = np.zeros((labels.__len__(), max(max(labels))), dtype='int')
        for i in range(labels.__len__()):
            for idx in labels[i]:
                target[i, idx-1] = 1
        return target


class TestMetric(fa.Callback):

    def on_epoch_begin(self, **kwargs):
        self.correct, self.total = 0, 0

    def on_batch_end(self, last_output, last_target, **kwargs):
        preds = last_output.argmax(1)
        self.correct += ((preds == 0) * (last_target == 0)).float().sum()
        self.total += (preds == 0).float().sum()

    def on_epoch_end(self, last_metrics, **kwargs):
        return fa.add_metrics(last_metrics, [self.correct / self.total])


def main():
    cl = ['Corner', 'Throwin', 'Shot']
    test_matches = ['CCP_LIB', 'RIV_BOC']
    event_dirs = os.listdir(DATASET)
    event_dirs.sort()
    train_evs, val_evs, test_evs = split_train_val_test(event_dirs, test_matches)
    tr, val, test = DataGenerator(train_evs, cl), DataGenerator(val_evs, cl), DataGenerator(test_evs, cl)
    data = fa.DataBunch.create(train_ds=tr, valid_ds=val, bs=64, test_ds=test)

    f_score=fa.partial(fa.fbeta, thresh=0.2)
    # learn = fa.cnn_learner(data, fa.models.resnet18, metrics=[fa.accuracy, fa.Precision(), fa.Recall(), TestMetric()])
    learn = fa.cnn_learner(data, fa.models.resnet18, metrics=[f_score])
    learn.unfreeze()
    try:
        learn.fit(1)
    except KeyboardInterrupt:
        print('ctrl c pressed.')

    learn.save('test3')

    print(learn.validate(data.test_dl))

    interp = fa.ClassificationInterpretation.from_learner(learn)
    # interp.plot_top_losses(6**2, figsize=(6, 6))
    interp.plot_confusion_matrix()
    plt.show()


if __name__ == "__main__":
    main()
