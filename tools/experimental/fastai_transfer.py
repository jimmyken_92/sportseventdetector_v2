from fastai import *
from fastai.vision import *
import matplotlib.pyplot as plt

bs = 64

# # Declaring path of dataset
# # Loading data
# data = ImageDataBunch.from_folder(path=path_img, train='train', valid='valid', ds_tfms=get_transforms(), size=224,
#                                   bs=bs, check_ext=False)
# # Normalizing data based on Image net parameters
# data.normalize(imagenet_stats)
# data.show_batch(rows=3, figsize=(10, 8))
# print(data.classes)
# len(data.classes), data.c

mnist = untar_data(URLs.PLANET_TINY)
# data = ImageDataBunch.from_folder(mnist)
planet_tfms = get_transforms(flip_vert=True, max_lighting=0.1, max_zoom=1.05, max_warp=0.)
data = (ImageList.from_csv(mnist, 'labels.csv', folder='train', suffix='.jpg')
        .split_by_rand_pct()
        .label_from_df(label_delim=' ')
        .databunch()
        )

recall = Recall()
prec = Precision()
learn = cnn_learner(data, models.resnet18)
learn.fit_one_cycle(1)


interp = ClassificationInterpretation.from_learner(learn)
interp.plot_top_losses(4, figsize=(20, 25))
interp.plot_confusion_matrix(figsize=(20, 20), dpi=60)
learn.lr_find()
learn.recorder.plot()
plt.show()

# learn.unfreeze()
# learn.fit_one_cycle(2, max_lr=slice(1e-7, 1e-5))
