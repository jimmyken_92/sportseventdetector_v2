from keras.models import model_from_json, load_model
from data_generator import DataGenerator
import numpy as np
import os
import random
from sklearn.metrics import classification_report, confusion_matrix
from shutil import copyfile
from pathlib import Path
import _pickle
from datetime import datetime
import ast
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import matplotlib.pyplot as plt
from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
import cv2
from keras import backend as K

def auc(y_true, y_pred):
    auc = auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
DATASET_FOLDER = 'dataset3/'
model_name = 'model7_ml'
test_matches = ['CCP_LIB']
train_params = {'epochs': 300, 'use_multiprocessing': False, 'shuffle': True, 'verbose': 1}
compile_params = {'loss': 'categorical_crossentropy', 'optimizer': 'adam', 'metrics': ['categorical_accuracy', ]}
# compile_params = {'loss': 'categorical_crossentropy', 'optimizer': 'adam', 'metrics': [auc ]}
batch_size = 64
val_pct = 0.2
no_of_events = 4
classes_bin = [0,1,2,3]

def plot_roc_auc(n_classes,y_score,y_test,try_name):
    lw = 2
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Compute macro-average ROC curve and ROC area
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    fig = plt.figure(figsize=(10, 8), dpi=100)
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average ROC curve (area = {0:0.2f})'
                   ''.format(roc_auc["micro"]),
             color='gold', linestyle=':', linewidth=4)

    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average ROC curve (area = {0:0.2f})'
                   ''.format(roc_auc["macro"]),
             color='darkgray', linestyle=':', linewidth=4)

    colors = cycle(['royalblue', 'firebrick', 'forestgreen', 'orchid'])
    for i, color in zip(range(n_classes), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                 label='ROC curve of class {0} (area = {1:0.2f})'
                       ''.format(i, roc_auc[i]))

    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.05])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    #plt.show()
    #plt.close()


def plot_confusion_matrix(y_true, y_pred, classes,normalize=False,title=None,cmap=plt.cm.Blues):
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    #classes = unique_labels(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax,cm


def split_train_val_test(evs, test_match, v_p=0.2):
    test_evs = []
    # Create test set
    evs2 = evs.copy()
    for ev in evs2:
        if any(match in ev for match in test_match):
            test_evs.append(ev)
            evs.remove(ev)
    # Split into classes
    classes = {}
    for ev in evs:
        cl = ast.literal_eval(ev.split('_')[0])
        if type(cl) is list:
            cl = cl[0]
        if cl not in classes.keys():
            classes[cl] = [ev]
        else:
            classes[cl].append(ev)
    # Find biggest class
    train_evs = []
    val_evs = []
    max_cl = 0
    for class_events in classes.values():
        split = int(len(class_events) * (1 - v_p))
        if split > max_cl:
            max_cl = split
    # Split into val and train and keep balance
    for class_events in classes.values():
        random.shuffle(class_events)
        split = int(np.ceil(len(class_events) * (1 - v_p)))
        times = int(np.round(max_cl / split))
        train_evs += times * class_events[:split]
        val_evs += class_events[split:]
    return train_evs, val_evs, test_evs


def test_model(t_matches, try_name):
    model = load_model(f'models/{try_name}/model.h5')
    event_dirs = os.listdir(DATASET_FOLDER)
    event_dirs.sort()
    _, _, test_evs = split_train_val_test(event_dirs, t_matches)
    use_color = True if model.layers[0].input.shape[4].value == 3 else False
    sz = (model.layers[0].input.shape[2].value, model.layers[0].input.shape[3].value)
    test_generator = DataGenerator(test_evs, DATASET_FOLDER, batch_size=test_evs.__len__(), use_color=use_color, dim=sz)

    y_pred = []
    for i in range(test_generator.list_paths.__len__()):
        inputs = test_generator.get_full_event(i)
        outputs = model.predict(inputs)
        outputs = np.average(outputs, axis=0)
        y_pred.append(outputs)

    y_pred = np.argmax(y_pred, axis=1)
    with open(f'models/{try_name}/results.txt', 'w') as f:
        f.write('Confusion Matrix\n')
        f.write(confusion_matrix(test_generator.classes(), y_pred).__str__())
        f.write('\nClassification Report\n')
        target_names = ['Foul', 'Corner', 'ThrowIn', 'Shot']
        f.write(classification_report(test_generator.classes(), y_pred, target_names=target_names))

    plot_confusion_matrix(test_generator.classes(), y_pred, classes=target_names, normalize=False)
    plt.savefig(f'models/{try_name}/Confusion_Matrix.jpg')

    plot_confusion_matrix(test_generator.classes(), y_pred, classes=target_names, normalize=True)
    plt.savefig(f'models/{try_name}/Normalized_Confusion_Matrix.jpg')

    y_test = label_binarize(test_generator.classes(), classes=classes_bin)
    # print(y_test)
    y_score = label_binarize(y_pred, classes=classes_bin)
    # print(y_score)
    plot_roc_auc(n_classes = no_of_events ,y_score = y_score, y_test=y_test, try_name = try_name)
    plt.savefig(f'models/{try_name}/ROC_AUC_test.jpg')

    with open(f'models/{try_name}/results.txt', 'r') as f:
        print(f.read())


def train(m_name, t_matches, try_name):
    event_dirs = os.listdir(DATASET_FOLDER)
    event_dirs.sort()

    # Create NN model
    model = model_from_json(open(f'models/{m_name}.json', 'r').read())
    model.compile(**compile_params)

    # Create generators
    use_color = True if model.layers[0].input.shape[4].value == 3 else False
    sz = (model.layers[0].input.shape[2].value, model.layers[0].input.shape[3].value)
    train_evs, val_evs, _ = split_train_val_test(event_dirs, t_matches, val_pct)
    train_generator = DataGenerator(train_evs, DATASET_FOLDER, batch_size=batch_size, use_color=use_color, dim=sz)
    val_generator = DataGenerator(val_evs, DATASET_FOLDER, batch_size=batch_size, use_color=use_color, dim=sz)
    try:
        model.fit_generator(train_generator, validation_data=val_generator, **train_params)
    except KeyboardInterrupt:
        print('\n Break detected. Wait to test the model.\n')

    train_score = []
    for i in range(train_generator.list_paths.__len__()):
        inputs = train_generator.get_full_event(i)
        outputs = model.predict(inputs)
        outputs = np.average(outputs, axis=0)
        train_score.append(outputs)
    #train_score = np.argmax(train_score, axis=1)
    #train_score = label_binarize(train_score, classes=classes_bin)
    y_train = label_binarize(train_generator.classes(), classes=classes_bin)
    plot_roc_auc(n_classes = no_of_events ,y_score = train_score, y_test=y_train, try_name = try_name)
    plt.savefig(f'models/{try_name}/ROC_AUC_train.jpg')

    val_score = []
    for i in range(val_generator.list_paths.__len__()):
        inputs = val_generator.get_full_event(i)
        outputs = model.predict(inputs)
        outputs = np.average(outputs, axis=0)
        val_score.append(outputs)
    val_score = np.argmax(val_score, axis=1)
    val_score = label_binarize(val_score, classes=classes_bin)
    y_val = label_binarize(val_generator.classes(), classes=classes_bin)
    plot_roc_auc(n_classes=no_of_events, y_score=val_score, y_test=y_val, try_name=try_name)
    plt.savefig(f'models/{try_name}/ROC_AUC_validation.jpg')

    model.save(f'models/{try_name}/model.h5')
    copyfile(f'models/{m_name}.json', f'models/{try_name}/model.json')
    copyfile('data_generator.py', f'models/{try_name}/data_generator.py')
    copyfile('football_conv_lstm.py', f'models/{try_name}/football_conv_lstm.py')
    with open(f'models/{try_name}/summary.txt', 'w') as f:
        model.summary(line_length=80, print_fn=f.write)


def main():
    global DATASET_FOLDER, model_name, test_matches, train_params, compile_params, batch_size, val_pct
    try_folder = datetime.now().__str__()
    #try_folder = '2019-08-08_01_48_03_654922'
    try_folder = try_folder.replace(" ", "_")
    try_folder = try_folder.replace(":", "_")
    try_folder = try_folder.replace(".", "_")
    new_path = Path(f'models/{try_folder}')
    if not new_path.exists():
        new_path.mkdir()
        with open(f'models/{try_folder}/params.pickle', 'wb') as ff:
            _pickle.dump((DATASET_FOLDER, model_name, test_matches, train_params, compile_params,
                          batch_size, val_pct), ff)
        train(model_name, test_matches, try_folder)
    else:
        with open(f'models/{try_folder}/params.pickle', 'rb') as ff:
            DATASET_FOLDER, model_name, test_matches, train_params, compile_params, batch_size, val_pct = \
                _pickle.load(ff)

    test_model(test_matches, try_folder)
    
    os.chdir(f'models/{try_folder}')
    im1 = cv2.imread('Confusion_Matrix.jpg')
    im2 = cv2.imread('Normalized_Confusion_Matrix.jpg')
    im_conc = cv2.vconcat([im1, im2])
    cv2.imwrite('Confusion_Matrices.jpg', im_conc)
    
    im1 = cv2.imread('ROC_AUC_train.jpg')
    im2 = cv2.imread('ROC_AUC_validation.jpg')
    im3 = cv2.imread('ROC_AUC_test.jpg')
    im_conc = cv2.vconcat([im1, im2, im3])
    cv2.imwrite('ROC_AUC_curves.jpg', im_conc)


if __name__ == "__main__":
    main()
