# -*- coding: utf-8 -*-
"""
Created on Mon Aug 5 10:28:44 2019

@author: listofbanned <listofbanned@gmail.com>
"""

# import fastai.vision as fa
# from keras.utils import plot_model
import numpy as np
from keras.utils import to_categorical
import cv2
from typing import List
import random
import os
from os import listdir
from os.path import isfile, join
from tools.experimantal.football_conv_lstm import split_train_val_test
from keras.utils import Sequence
import ast
from keras.models import model_from_json, load_model
from sklearn.metrics import classification_report, confusion_matrix
from shutil import copyfile
from pathlib import Path
import _pickle
from datetime import datetime
from classification_models.resnet import preprocess_input
from tools.layer_tools import get_resnet50, get_inceptionv3, set_model_layers_tranable


DATASET = 'dataset3/'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
DATASET_FOLDER = 'dataset3/'
model_name = 'ResNet50'
test_matches = ['CCP_LIB', 'CCP_CASLA'] # TEST_MATCH_1
train_params = {'epochs': 200, 'use_multiprocessing': False, 'shuffle': True, 'verbose': 1}
compile_params = {'loss': 'categorical_crossentropy', 'optimizer': 'adam', 'metrics': ['categorical_accuracy', ]}
batch_size = 32
val_pct = 0.2
n_channels = 6
n_classes = 4

class DataGenerator(Sequence):
    def __init__(self, list_paths: List, dataset: str, batch_size: int = 32, dim: tuple = (224, 224),
                 shuffle: bool = True, skip: int = 15, past_frames: int = 3, use_color: bool = False):

        self.dim = dim
        self.batch_size = batch_size
        self.list_paths = list_paths
        self.shuffle = shuffle
        self.skip = skip
        self.past_frames = past_frames
        self.labels = []
        self.use_color = use_color
        self.dataset = dataset
        self.on_epoch_end()

    def classes(self):
        return np.argmax(self.labels, axis=1)

    def get_full_event(self, event_id):
        full_event = []
        event_name = self.list_paths[event_id]
        images = [f for f in listdir(DATASET + event_name) if isfile(join(DATASET + event_name, f))]
        for start_frame in range(images.__len__() - self.past_frames * self.skip - 1):
            event = self.get_event_chunk(event_name, start_frame)
            event = np.stack(event, axis=0)
            full_event.append(event)
        full_event = np.stack(full_event, axis=0)
        #        if not self.use_color:
        #            full_event = np.expand_dims(full_event, axis=4)
        return full_event

    def get_event_chunk(self, path, start_frame):
        event = []
        for i in range(start_frame, start_frame + self.skip * self.past_frames, self.skip):
            if not self.use_color:
                im = cv2.imread(DATASET + path + f'/{i}.jpg', 0)
            else:
                im = cv2.imread(DATASET + path + f'/{i}.jpg')
            im = cv2.resize(im, self.dim, None)
            event.append(im)
        return event

    def __len__(self):
        return int(np.floor(len(self.list_paths) / self.batch_size))

    def __getitem__(self, index):
        list_paths = self.list_paths[index * self.batch_size:(index + 1) * self.batch_size]
        y = self.labels[index * self.batch_size:(index + 1) * self.batch_size, :]
        x = []
        for path in list_paths:
            # y = self.labels[index]
            number_of_ims = os.listdir(DATASET + path).__len__() - 1
            start_frame = np.random.randint(number_of_ims - self.skip * self.past_frames)
            events = self.get_event_chunk(path, start_frame)
            x.append(events)
        x = np.reshape(x, (batch_size, 224, 224, n_channels))
        x = np.array(x)
        # print(x.shape)
        # x = np.expand_dims(x, axis=4)
        return x, y

    def on_epoch_end(self):
        # print("list_paths:",self.list_paths)
        random.shuffle(self.list_paths)
        labels = []
        for path in self.list_paths:
            # convert string that has an array inside , it makes to
            labels.append(ast.literal_eval(path.split('_')[0]))

        if type(labels[0]) is not list:
            self.labels = to_categorical(labels)
        else:
            self.labels = self.to_multicalss_categorical(labels)
        idx = np.argwhere(np.all(self.labels[..., :] == 0, axis=0))
        # print("idx:",idx)
        self.labels = np.delete(self.labels, idx, axis=1)

    def to_multicalss_categorical(self, labels):
        target = np.zeros((labels.__len__(), max(max(labels))))
        for i in range(labels.__len__()):
            for idx in labels[i]:
                target[i, idx - 1] = 1.0
        return target


def test_model(t_matches, try_name):
    model = load_model(f'models/{try_name}/model.h5')
    event_dirs = os.listdir(DATASET_FOLDER)
    event_dirs.sort()
    _, _, test_evs = split_train_val_test(event_dirs, t_matches)
    print(test_evs)
    # use_color = True if model.layers[0].input.shape[4].value == 3 else False
    use_color = False
    # sz = (model.layers[0].input.shape[2].value, model.layers[0].input.shape[3].value)
    sz = (224, 224)
    test_generator = DataGenerator(test_evs, DATASET_FOLDER, batch_size=test_evs.__len__(), use_color=use_color, dim=sz)

    y_pred = []
    for i in range(test_generator.list_paths.__len__()):
        inputs = test_generator.get_full_event(i)
        inputs = preprocess_input(inputs)
        # inputs = np.expand_dims(inputs,0)
        inputs = np.reshape(inputs, (154, 224, 224, 3))
        outputs = model.predict(inputs)
        outputs = np.average(outputs, axis=0)
        y_pred.append(outputs)
    y_pred = np.argmax(y_pred, axis=1)
    with open(f'models/{try_name}/results.txt', 'w') as f:
        f.write('Confusion Matrix\n')
        f.write(confusion_matrix(test_generator.classes(), y_pred).__str__())
        f.write('\nClassification Report\n')
        target_names = ['Corner', 'Shoot']
        f.write(classification_report(test_generator.classes(), y_pred, target_names=target_names))
    with open(f'models/{try_name}/results.txt', 'r') as f:
        print(f.read())


def train(m_name, t_matches, try_name):
    event_dirs = os.listdir(DATASET_FOLDER)
    event_dirs.sort()
    if model_name == 'ResNet50':
        model = get_resnet50(n_channels,n_classes)
        set_model_layers_tranable(model,0,1,True)
        set_model_layers_tranable(model,1,175,False)
        set_model_layers_tranable(model,175,177,True)
    elif(model_name == 'InceptionV3'):
        model = get_inceptionv3(n_channels, n_classes)
    else:
        model = model_from_json(open(f'models/{m_name}.json', 'r').read())
    # plot_model(model, to_file='resnet_18.jpg')
    model.compile(**compile_params)

    # Create generators
    # use_color = True if model.layers[0].input.shape[4].value == 3 else False
    use_color = False
    # sz = (model.layers[0].input.shape[2].value, model.layers[0].input.shape[3].value)
    sz = (224, 224)
    train_evs, val_evs, _ = split_train_val_test(event_dirs, t_matches, val_pct)
    train_generator = DataGenerator(train_evs, DATASET_FOLDER, batch_size=batch_size, use_color=use_color, dim=sz, past_frames=n_channels)
    val_generator = DataGenerator(val_evs, DATASET_FOLDER, batch_size=batch_size, use_color=use_color, dim=sz, past_frames=n_channels)
    try:
        model.fit_generator(train_generator, validation_data=val_generator, **train_params)
    except KeyboardInterrupt:
        print('\n Braek detected. Wait to test the model.\n')
    model.save(f'models/{try_name}/model.h5')
    copyfile(f'models/{m_name}.json', f'models/{try_name}/model.json')
    copyfile('keras_test.py', f'models/{try_name}/keras_test.py')
    copyfile('football_conv_lstm.py', f'models/{try_name}/football_conv_lstm.py')
    with open(f'models/{try_name}/summary.txt', 'w') as f:
        model.summary(line_length=80, print_fn=f.write)


def main():
    global DATASET_FOLDER, model_name, test_matches, train_params, compile_params, batch_size, val_pct

    try_folder = datetime.now().__str__()
    try_folder = try_folder.replace(" ", "_")
    try_folder = try_folder.replace(":", "_")
    try_folder = try_folder.replace(".", "_")

    new_path = Path(f'models/{try_folder}')
    if not new_path.exists():
        new_path.mkdir()
        with open(f'models/{try_folder}/params.pickle', 'wb') as ff:
            _pickle.dump((DATASET_FOLDER, model_name, test_matches, train_params, compile_params,
                          batch_size, val_pct), ff)
        train(model_name, test_matches, try_folder)
    else:
        with open(f'models/{try_folder}/params.pickle', 'rb') as ff:
            print(os.getcwd())
            DATASET_FOLDER, model_name, test_matches, train_params, compile_params, batch_size, val_pct = \
                _pickle.load(ff)

    test_model(test_matches, try_folder)


if __name__ == "__main__":
    main()
