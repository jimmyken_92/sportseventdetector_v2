import os
from tools.layer_tools import get_resnet50

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '5'

model = get_resnet50(4, 4)
model.summary()