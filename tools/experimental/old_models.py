from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, AveragePooling3D, AveragePooling2D, MaxPooling3D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization

def create_model():
    use_dropout = True
    model = Sequential()
    model.add(ConvLSTM2D(filters=40, kernel_size=(3, 3),
                       input_shape=(None, 32, 32, 1),
                       padding='same', return_sequences=True))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())
    if use_dropout:
        model.add(Dropout(0.3))
    model.add(ConvLSTM2D(filters=40, kernel_size=(3, 3),
                       padding='same', return_sequences=False))
    model.add(AveragePooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Flatten())
    if use_dropout:
        model.add(Dropout(0.3))
    model.add(Dense(units=100, activation='tanh', use_bias=True))
    if use_dropout:
        model.add(Dropout(0.3))
    model.add(Dense(units=51, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy',])
    return model