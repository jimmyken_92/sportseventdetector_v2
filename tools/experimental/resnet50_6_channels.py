import os
from keras.models import model_from_json, load_model
from keras.models import Model
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Input, Flatten
from keras.layers.convolutional import Conv2D, ZeroPadding2D


orig_model = ResNet50(include_top=False, weights='imagenet')
orig_model.summary()
# print(orig_model.input)
# orig_model.input = Input(shape=(64, 224, 224, 6))
print(orig_model.input)
print(orig_model.layers[0])
orig_model.layers[0] = Input(shape=(224, 224, 6))
#orig_model.layers[0] = Conv2D(3, 3, padding='same')
print(orig_model.layers[0])
#orig_model.layers[1] = ZeroPadding2D(padding=(3,3))
orig_model.layers[1] = Conv2D(3, 3, padding = 'same')
orig_model.layers[2] = Conv2D(3, 3, padding='same')
print(orig_model.layers[0:4])

# orig_model.save('resnet50_channels_modified.h5')
# model_json = orig_model.to_json()
# with open("resnet50_channels_modified.json", "w") as json_file:
#     json_file.write(model_json)
#
orig_model.summary()

# Add FC for transfer learning
#base_model = ResNet50(weights='imagenet', include_top=False)
base_model = orig_model
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(1024, activation='relu')(x)
predictions = Dense(2, activation='sigmoid')(x)

model = Model(base_model.input, outputs=predictions)

for layer in base_model.layers:
    layer.trainable = False

for i, layer in enumerate(base_model.layers):
    print(i, layer.name)

for layer in model.layers[:175]:
    layer.trainable = False
for layer in model.layers[175:]:
    layer.trainable = True

print(model.layers[175:])

model_json = model.to_json()
with open("resnet50_channels_modified.json", "w") as json_file:
    json_file.write(model_json)


orig_model.layers.pop(0)
newInput = Input(batch_shape=(0,224,224,6))    # let us say this new InputLayer
newOutputs = orig_model(newInput)
newModel = Model(newInput, newOutputs)

