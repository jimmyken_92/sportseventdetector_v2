from dataset_creator import MatchEventExtractor
import numpy as np
import pandas as pd
import cv2
import argparse
import os

os.chdir('..')

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--game", help="Choose game")
args = parser.parse_args()

match = args.game
print(match)
cap = cv2.VideoCapture(f'data_raw/{match}/{match}_compressed.mp4')

fps = int(cap.get(cv2.CAP_PROP_FPS))
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

extractors = [MatchEventExtractor(match, [20, 30, 36, 100], [101, 1702],  'dataset/', 'data_raw', True,
                                  sequence_length=6, consolidate_period=6)]

events = [ex.get_match_events() for ex in extractors]

final_csv = np.zeros(total_frames*8).reshape(total_frames,8)
for i in range(0,total_frames):
    final_csv[i,0]=i

for i in range(0,len(events[0])):
    if (events[0][i][0]['subEventId'] == 0):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2,1]=1
        b = -4*fps
        c = 2*fps
        for k in range(a2+b,a2):
            final_csv[k, 1] = 1
        for l in range(a2,a2+c):
            final_csv[l, 1] = 1
    if (events[0][i][0]['subEventId'] == 20):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 2] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 2] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 2] = 1
    if (events[0][i][0]['subEventId'] == 30):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 3] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 3] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 3] = 1
    if (events[0][i][0]['subEventId'] == 36):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 4] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 4] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 4] = 1
    if (events[0][i][0]['subEventId'] == 100):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 5] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 5] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 5] = 1
    if (events[0][i][0]['subEventId'] == 101):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 6] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 6] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 6] = 1
    if (events[0][i][0]['subEventId'] == 1702):
        a = int(events[0][i][0]['eventSec'])
        a2 = a*fps
        final_csv[a2, 7] = 1
        b = -4 * fps
        c = 2 * fps
        for k in range(a2 + b, a2):
            final_csv[k, 7] = 1
        for l in range(a2, a2 + c):
            final_csv[l, 7] = 1
'''
# timestamp
# noEvent
# foul
# corner
# throwIn
# shot
# goal
# yellowCard
'''
df = pd.DataFrame(data=final_csv,columns=['timestamp','noEvent','foul','corner','throwIn','shot','goal','yellowCard'])
df.to_csv(f'resources/trueL_{match}.csv',index=False,header=True)
