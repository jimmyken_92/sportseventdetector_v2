import re
from keras import layers
import keras.applications as apps
from keras.models import Model
import numpy as np


def insert_layer_nonseq(model, input_shape, layer_regex, insert_layer_factory,
                        insert_layer_name=None, position='replace'):
    x = None
    # Auxiliary dictionary to describe the network graph
    network_dict = {'input_layers_of': {}, 'new_output_tensor_of': {}}

    # Set the input layers of each layer
    for layer in model.layers:
        for node in layer._outbound_nodes:
            layer_name = node.outbound_layer.name
            if layer_name not in network_dict['input_layers_of']:
                network_dict['input_layers_of'].update(
                    {layer_name: [layer.name]})
            else:
                network_dict['input_layers_of'][layer_name].append(layer.name)

    img_input = layers.Input(shape=input_shape, name=model.layers[0].name)
    # Set the output tensor of the input layer
    network_dict['new_output_tensor_of'].update(
        {model.layers[0].name: img_input})

    # Iterate over all layers after the input
    for layer in model.layers[1:]:

        # Determine input tensors
        layer_input = [network_dict['new_output_tensor_of'][layer_aux]
                       for layer_aux in network_dict['input_layers_of'][layer.name]]
        if len(layer_input) == 1:
            layer_input = layer_input[0]

        # Insert layer if name matches the regular expression
        if re.match(layer_regex, layer.name):
            if position == 'replace':
                x = layer_input
            elif position == 'after':
                x = layer(layer_input)
            elif position == 'before':
                pass
            else:
                raise ValueError('position must be: before, after or replace')

            new_layer = insert_layer_factory()

            if insert_layer_name:
                new_layer.name = insert_layer_name
            else:
                new_layer.name = '{}_{}'.format(layer.name,
                                                new_layer.name)
            x = new_layer(x)
            print('Layer {} inserted after layer {}'.format(new_layer.name,
                                                            layer.name))
            if position == 'before':
                x = layer(x)

            w = np.array(layer.get_weights()[0])
            w = np.repeat(w, int(np.ceil(input_shape[2] / w.shape[2])), axis=2)
            w = w[:, :, :input_shape[2], :]
            new_w = [w.__array__()]

            if layer.use_bias:
                b = layer.get_weights()[1]
                new_w.append(b)
            new_layer.set_weights(new_w)

        else:
            x = layer(layer_input)

        # Set new output tensor (the original one, or the one of the inserted
        # layer)
        network_dict['new_output_tensor_of'].update({layer.name: x})

    return Model(inputs=img_input, outputs=x)


def set_model_layers_tranable(model, start, stop, trainable):
    for i in range(start, stop):
        model.layers[i].trainable = trainable


def get_resnet50(channels, classes):
    base_model = apps.ResNet50(include_top=False, weights='imagenet')
    x = layers.GlobalAveragePooling2D(name='avg_pool')(base_model.output)
    x = layers.Dense(classes, activation='sigmoid', name='fc')(x)
    model = Model(base_model.input, x)

    def conv_layer():
        return layers.Conv2D(64, (7, 7), strides=(2, 2), padding='valid', kernel_initializer='he_normal', name='conv1')

    model = insert_layer_nonseq(model, (None, None, channels), 'conv1$', conv_layer)
    set_model_layers_tranable(model, 4, model.layers.__len__() - 2, False)
    return model


def get_inceptionv3(channels, classes):
    base_model = apps.InceptionV3(include_top=False, weights='imagenet')
    x = layers.GlobalAveragePooling2D(name='avg_pool')(base_model.output)
    x = layers.Dense(classes, activation='sigmoid', name='predictions')(x)
    model = Model(base_model.input, x)

    def conv_layer():
        return layers.Conv2D(32, (3, 3), strides=(2, 2), padding='valid', use_bias=False)

    model = insert_layer_nonseq(model, (None, None, channels), 'conv2d_1$', conv_layer)
    # plot_model(model, show_shapes=True)
    set_model_layers_tranable(model, 2, model.layers.__len__() - 2, False)
    return model


def unfreeze(model):
    for layer in model.layers:
        layer.trainable = True


def freeze(model):
    for layer in model.layers:
        layer.trainable = False
