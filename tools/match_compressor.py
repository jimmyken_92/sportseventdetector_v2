import os
import shutil
from pathlib import Path
from dataset_creator import get_all_matches


data_raw = 'data_raw'
candidates_folder = '../temp'

m_candidates = get_all_matches(candidates_folder)
m_processed = get_all_matches(data_raw)

data_raw_path = Path.cwd() / data_raw
candidate_path = Path(candidates_folder)

for match in m_candidates:
    compressed = data_raw_path / match / (match + '_compressed.mp4')
    if not compressed.exists():
        in_file = candidate_path / match / (match + '.mp4')
        if not in_file.exists():
            print(f'No video file for {match}')
            shutil.rmtree(candidate_path / match)
            continue
        shutil.rmtree(data_raw_path / match, ignore_errors=True)
        out_file = candidate_path / match / (match + '_compressed.mp4')
        if out_file.exists():
            print(f'Compressed already exists for {match}')
        else:
            command = f'ffmpeg -i {in_file} -y -strict -2 -filter:v fps=15 {out_file}'
            print(command)
            os.system(command)
        shutil.copytree(candidate_path/match, data_raw_path/match)
    else:
        shutil.rmtree(candidate_path / match)
        print(f'Already processed: {match}')
