import numpy as np
from sklearn.metrics import confusion_matrix
from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn.metrics import roc_curve, auc, average_precision_score, multilabel_confusion_matrix, accuracy_score
from tqdm import tqdm
import cv2
from keras.applications.resnet50 import preprocess_input

# for each one of the training/validation/test sets, plot all the classes with their corresponding roc_auc_scores and
# average precision per class
def plot_roc_auc(y_score, y_test, labels, filename):
    lw = 2
    n_classes = y_test.shape[1]
    y_score = np.array(y_score)
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Compute macro-average ROC curve and ROC area
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    fig = plt.figure(figsize=(10, 8), dpi=100)
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average ROC curve (area = {0:0.2f})'
                   ''.format(roc_auc["micro"]),
             color='gold', linestyle=':', linewidth=4)

    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average ROC curve (area = {0:0.2f})'
                   ''.format(roc_auc["macro"]),
             color='darkgray', linestyle=':', linewidth=4)
    apr_score = []
    for i in range(0,n_classes):
        temp_avg_score = average_precision_score(y_test[:,i], y_score[:,i], average=None)
        apr_score.append(temp_avg_score)
    apr_score = np.array(apr_score)

    #colors = cycle(['royalblue', 'firebrick', 'forestgreen', 'orchid'])

    #for i, color in zip(range(n_classes), colors):
    for i in range(0,n_classes):
        plt.plot(fpr[i], tpr[i], lw=lw,
                 label='ROC curve of class {0} (area = {1:0.2f}) (avg_pre={2:0.2f})'
                       ''.format(str(labels[i]), roc_auc[i], apr_score[i]))

    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.axis('scaled')  # scaled, square
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    plt.savefig(filename)

# returns a plot for each class that contains corresponding training/validation/test set roc_auc_scores
def plot_roc_auc_per_class(y_train, train_score, y_val, val_score, y_test, test_score, labels, label_select, filename):
    y_test = np.array(y_test)
    test_score = np.array(test_score)
    y_val = np.array(y_val)
    val_score = np.array(val_score)
    y_train = np.array(y_train)
    train_score = np.array(train_score)

    # Plot linewidth.
    lw = 2
    n_classes = y_test.shape[1]
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    fpr[0], tpr[0], _ = roc_curve(y_train[:, label_select], train_score[:, label_select])
    roc_auc[0] = auc(fpr[0], tpr[0])

    fpr[1], tpr[1], _ = roc_curve(y_val[:, label_select], val_score[:, label_select])
    roc_auc[1] = auc(fpr[1], tpr[1])

    fpr[2], tpr[2], _ = roc_curve(y_test[:, label_select], test_score[:, label_select])
    roc_auc[2] = auc(fpr[2], tpr[2])

    fig = plt.figure(figsize=(10, 8), dpi=100)

    temp = ['Train', 'Validation', 'Test']

    #colors = cycle(['royalblue', 'firebrick', 'forestgreen', 'orchid'])
    #for i, color in zip(range(n_classes - 1), colors):
    for i in range(0,3):
        plt.plot(fpr[i], tpr[i], lw=lw,
                 label='{0} ROC Curve of class {1} (auc area = {2})'
                                                           ''.format(temp[i], str(labels[label_select]),
                                                                     np.round(roc_auc[i], 2)))

    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.axis('scaled')  # scaled, square
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    plt.savefig(filename)

# returns one confusion matrix after the prediction step on test set
def plot_confusion_matrix(cm, classes, normalize):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[0]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    cmap = plt.cm.Blues
    title = 'Confusion matrix'
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()

# concatenate plots and save in one .jpg file
def concatenate_plots(folder, n_plots, target_names):
    # im1 = cv2.imread(f'models/{folder}/{target_names[n_plots - 4]}/vs_rest.jpg')
    # im2 = cv2.imread(f'models/{folder}/{target_names[n_plots - 3]}/vs_rest.jpg')
    # im3 = cv2.imread(f'models/{folder}/{target_names[n_plots - 2]}/vs_rest.jpg')
    # im4 = cv2.imread(f'models/{folder}/{target_names[n_plots - 1]}/vs_rest.jpg')
    #
    # im_conc = cv2.vconcat([im1, im2, im3, im4])
    # cv2.imwrite(f'models/{folder}/Confusion_Matrices.jpg', im_conc)
    #
    # im1 = cv2.imread(f'models/{folder}/{target_names[n_plots - 4]}/vs_rest_normalized.jpg')
    # im2 = cv2.imread(f'models/{folder}/{target_names[n_plots - 3]}/vs_rest_normalized.jpg')
    # im3 = cv2.imread(f'models/{folder}/{target_names[n_plots - 2]}/vs_rest_normalized.jpg')
    # im4 = cv2.imread(f'models/{folder}/{target_names[n_plots - 1]}/vs_rest_normalized.jpg')
    #
    # im_conc = cv2.vconcat([im1, im2, im3, im4])
    # cv2.imwrite(f'models/{folder}/Normalized_Confusion_Matrices.jpg', im_conc)

    im1 = cv2.imread(f'models/{folder}/ROC_AUC_train.jpg')
    im2 = cv2.imread(f'models/{folder}/ROC_AUC_validation.jpg')
    im3 = cv2.imread(f'models/{folder}/ROC_AUC_test.jpg')
    im_conc = cv2.vconcat([im1, im2, im3])
    cv2.imwrite(f'models/{folder}/ROC_AUC_curves.jpg', im_conc)


# get predicted values when given as input the y values of training/validation/test set to the model
def get_full_scores(model, generator):
    score = []
    for i in tqdm(range(generator.list_paths.__len__())):
        inputs = generator.get_full_event(i)
        outputs = model.predict(inputs)
        outputs = np.average(outputs, axis=0)
        score.append(outputs)
    return score

# return the optimal threshold value for each class
def threshold_selector(test_labels, test_score, label):
    thresholds = np.arange(0.05, 1.00, 0.05)
    no_of_samples = test_score.shape[0]
    y_pred = np.zeros(no_of_samples * 1).reshape(no_of_samples, 1)
    score = np.zeros(len(thresholds) * 1).reshape(len(thresholds), 1)
    for i in range(0, len(thresholds)):
        y_pred = test_score >= thresholds[i]
        score[i] = accuracy_score(test_labels, y_pred)

    # print(score)
    position = 0
    max = score[0]
    for i in range(0, 18):
        if (score[i] > max):
            max = score[i]
            position = i
    optimal_threshold = np.round(thresholds[position],2)
    return optimal_threshold
