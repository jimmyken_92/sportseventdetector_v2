import numpy as np
import cv2
import yaml
from fastai.vision import load_learner, pil2tensor, Image
import PIL
from pathlib import Path
from src.draw.cv2 import draw_text


# Load config
with open('params.yaml', 'r') as stream:
    config = yaml.safe_load(stream)
model_dir = Path(config['inference_sb']['model_dir'])
vid_name = Path(config['inference_sb']['vid_name'])
data_dir = Path(config['inference_sb']['data_dir'])
pred_rate_frames = config['inference_sb']['pred_rate_frames']
l_buff_div = config['inference_sb']['l_buff_div']

# Load video
cap = cv2.VideoCapture(str(data_dir/vid_name))
ret, frame = cap.read()
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
assert total_frames, 'There are no frames in the video path'
h, w, c = frame.shape
l_buff = w//l_buff_div
fps = cap.get(cv2.CAP_PROP_FPS)
frame_pos = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
cap.set(cv2.CAP_PROP_POS_FRAMES, fps*60*0)


# Load inference model
print('Loading learner...')
learn = load_learner(model_dir, file='learn_export_sb.pkl')
learn.to_fp32()

# Run inference on video
frame_pos_all = []
preds = []
while True:
    ret, frame = cap.read()
    if not ret: break
    height, width, channel = frame.shape
    frame_sb = frame[:h // 2, l_buff:l_buff + h // 2].copy()
    frame_sb_rgb = cv2.cvtColor(frame_sb, cv2.COLOR_BGR2RGB)
    frame_pos = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    if not frame_pos % pred_rate_frames == 0:
        continue

    img_torch = Image(pil2tensor(PIL.Image.fromarray(frame_sb_rgb),
                                 np.float32).div(255))
    pred = learn.predict(img_torch)
    preds.append(list(pred[2].numpy()))
    frame_pos_all.append(frame_pos)
    print(frame_pos, [round(i) for i in pred[2].numpy()], pred[0].obj)
    draw_text(frame, frame_pos, (50, 50), 2)
    cv2.imshow('orig', frame)
    cv2.imshow('sb', frame_sb)
    cv2.waitKey(0)
    # cv2.imwrite(str(data_dir/'snapshots'/f'{vid_name.stem}_{frame_pos}.jpg'), frame_sb)

cap.release()
cv2.destroyAllWindows()

# Save results
classes = learn.data.classes
with open(data_dir/'classes.txt', 'w', newline='') as f:
    for c in classes:
        f.write(f'{c}\n')

csv_out = []
for i in range(len(frame_pos_all)):
    csv_out.append([frame_pos_all[i]] + [j for j in preds[i]])
np.savetxt(data_dir/'sb_preds.csv', np.array(csv_out), fmt='%1.3f', delimiter=',')
print('Classes and predictions saved to: ', data_dir)
