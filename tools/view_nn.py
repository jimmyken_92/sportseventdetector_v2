from collections import deque
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt
import pickle
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import Adam


np.random.seed(0)

def get_data(match):
    train_input = np.genfromtxt(f'resources/pred_{match}.csv', delimiter=',', skip_header=1)
    train_input_view = np.genfromtxt(f'resources/camview_pred_{match}.csv', delimiter=',', skip_header=0)
    train_labels = np.genfromtxt(f'resources/trueL_{match}.csv', delimiter=',', skip_header=1)

    t_in = []
    t_out = []

    buffer = deque(maxlen=15)
    if match == 'LIB_CCP':
        stride = 1
    else:
        stride = 2
    for i in range(0, len(train_input_view) - 1, stride):
        buffer.append(train_input_view[i])
        if len(buffer) == 15:
            buffer2 = buffer.copy()
            for j in range(len(buffer2)):
                idx = np.where(train_labels[:, 0] == buffer2[j][0])[0][0]
                buffer2[j] = np.concatenate((buffer2[j], train_input[idx, :]))
            t_in.append(np.array(buffer2)[:, 1:].ravel())
            t_out.append(train_labels[idx, 1:])
    t_in = np.array(t_in)
    t_out = np.array(t_out)
    t_out = t_out[:, 1:]
    return t_in, t_out


def train(t_in, t_out, test_in, test_out):
    model = Sequential([
        Dense(64, input_shape=(240,)),
        LeakyReLU(),
        Dense(6, activation='sigmoid'),
    ])

    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)
    chk = ModelCheckpoint(filepath=f'resources/model3.h5', monitor='val_loss', mode='min', save_best_only=True,
                          verbose=1)

    lr_red = ReduceLROnPlateau(monitor='val_loss', factor=0.33, patience=5, min_lr=0, verbose=1)

    opt = Adam(lr=0.001)
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['binary_accuracy'])
    model.fit(t_in, t_out, epochs=30, batch_size=64, validation_data=(test_in, test_out), callbacks=[es, chk, lr_red],
              shuffle=True)
    return model

# t_in = np.concatenate((t_in1, t_in2))
# t_out = np.concatenate((t_out1, t_out2))

# with open('tmp.pickle', 'wb') as f:
#     pickle.dump([t_in, t_out, test_in, test_out], f)

with open("tmp.pickle", "rb") as f:
    t_in, t_out, test_in, test_out = pickle.load(f)

model = train(t_in, t_out, test_in, test_out)
a = model.predict(test_in)

for i in range(6):
    plt.plot(a[:, i] > 0.5)
    plt.show()
    plt.plot(test_out[:, i])
    plt.show()
print()
# TODO evaluate model
